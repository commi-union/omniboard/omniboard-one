package tk.labyrinth.omniboardone.iam;

import jakarta.enterprise.inject.Instance;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import tk.labyrinth.pandora.iam.domain.userstate.UserStateHandler;

@RequiredArgsConstructor
@Singleton
public class AccessManager {

	private final Instance<UserStateHandler> userStateHandlerInstance;

	@ConfigProperty(name = "omniboard.admins", defaultValue = "foo")
	private String admins;

	public boolean isCurrentUserAdmin() {
		return userStateHandlerInstance.get().getUserState().getAuthenticationInformation() != null;
	}
}
