package tk.labyrinth.omniboardone.iam;

import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.simple.secret.Secret;
import tk.labyrinth.pandora.iam.domain.useraccount.UserAccount;
import tk.labyrinth.pandora.iam.domain.useraccount.UserAccountRepository;

import java.util.UUID;

// TODO: Rework into startup function.
@RequiredArgsConstructor
@Singleton
public class AdminUserAccountInitializer {

	private final UserAccountRepository userAccountRepository;

	private void onStartup(@Observes StartupEvent ignored) {
		if (userAccountRepository.count() == 0) {
			userAccountRepository.persist(UserAccount.builder()
					.displayName("Admin")
					.password(Secret.of("admin"))
					.uid(UUID.randomUUID())
					.username("admin")
					.build());
		}
	}
}
