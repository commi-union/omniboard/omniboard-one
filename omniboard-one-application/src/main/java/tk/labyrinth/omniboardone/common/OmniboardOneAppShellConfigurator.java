package tk.labyrinth.omniboardone.common;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.theme.Theme;

@CssImport("./style/pandora.css")
@CssImport("./style/pandora-lumo.css")
@CssImport("./style/pandora-colours.css")
@Theme("pandora-theme")
public class OmniboardOneAppShellConfigurator implements AppShellConfigurator {
	// empty
}
