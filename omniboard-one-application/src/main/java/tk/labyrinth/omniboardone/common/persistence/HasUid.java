package tk.labyrinth.omniboardone.common.persistence;

import java.util.UUID;

public interface HasUid<T> {

	UUID getUid();

	T withUid(UUID uid);
}
