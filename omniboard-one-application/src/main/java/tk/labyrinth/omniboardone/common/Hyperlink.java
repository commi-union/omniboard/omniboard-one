package tk.labyrinth.omniboardone.common;

import jakarta.annotation.Nullable;
import lombok.Value;
import lombok.With;

import java.util.Comparator;

// FIXME: This is not a simple type, need to relocate it.
@Value(staticConstructor = "of")
@With
public class Hyperlink {

	public static final Comparator<Hyperlink> COMPARATOR = Comparator.comparing(
			Hyperlink::getText,
			Comparator.nullsLast(Comparator.naturalOrder()));

	@Nullable
	String destination;

	@Nullable
	String text;
}
