package tk.labyrinth.omniboardone.util;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.treegrid.TreeGrid;
import tk.labyrinth.omniboardone.common.persistence.HasUid;
import tk.labyrinth.omniboardone.tool.RepositoryBase;

import java.util.Objects;
import java.util.UUID;

public class ViewUtils {

	public static <T> Grid<T> createGrid() {
		Grid<T> grid = new Grid<>();
		{
			grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
		}
		return grid;
	}

	public static <T> TreeGrid<T> createTreeGrid() {
		TreeGrid<T> grid = new TreeGrid<>();
		{
			grid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
		}
		return grid;
	}

	public static <T extends HasUid<T>> void processFormConfirm(
			boolean isAdmin,
			RepositoryBase<T> repository,
			T initialValue,
			T currentValue) {
		if (isAdmin && !Objects.equals(currentValue, initialValue)) {
			repository.persistOrUpdate(
					currentValue.withUid(initialValue.getUid() != null ? initialValue.getUid() : UUID.randomUUID()));
			//
			// TODO: We just need to refresh the content here.
			UI.getCurrent().getPage().reload();
		}
	}
}
