package tk.labyrinth.omniboardone.util;

import com.mongodb.MongoClientSettings;
import org.bson.BsonDocument;
import org.bson.BsonReader;
import org.bson.codecs.BsonDocumentCodec;
import org.bson.codecs.DecoderContext;

public class BsonUtils {

	public static BsonDocument read(BsonReader reader, DecoderContext decoderContext) {
		return new BsonDocumentCodec(MongoClientSettings.getDefaultCodecRegistry()).decode(reader, decoderContext);
	}
}
