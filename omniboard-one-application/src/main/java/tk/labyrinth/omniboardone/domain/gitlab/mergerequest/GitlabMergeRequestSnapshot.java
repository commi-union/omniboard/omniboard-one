package tk.labyrinth.omniboardone.domain.gitlab.mergerequest;

import jakarta.annotation.Nullable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.With;
import lombok.experimental.FieldDefaults;
import org.bson.codecs.pojo.annotations.BsonId;
import org.gitlab4j.api.models.ApprovalState;
import org.gitlab4j.api.models.MergeRequest;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSignature;

import java.util.UUID;

@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@With
public class GitlabMergeRequestSnapshot {

	@EqualsAndHashCode.Exclude
	@Nullable
	ApprovalState approvalState;

	@EqualsAndHashCode.Exclude
	@NonNull
	MergeRequest mergeRequest;

	@lombok.Builder.Default
	@NonNull
	String mergeRequestSignature = "NONE";

	@NonNull
	GitlabProjectSignature projectSignature;

	@BsonId
	UUID uid;
}
