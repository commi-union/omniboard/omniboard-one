package tk.labyrinth.omniboardone.domain.gitlab;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.gitlab4j.api.models.Environment;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class GitlabEnvironmentSnapshot {

	Long gitlabId;

	Long lastDeploymentGitlabId;

	String name;

	String slug;

	Environment.EnvironmentState state;
}
