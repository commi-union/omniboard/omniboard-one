package tk.labyrinth.omniboardone.domain.gitlab.mergerequest;

import io.quarkus.mongodb.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import io.vavr.collection.List;
import jakarta.annotation.Nullable;
import jakarta.enterprise.context.ApplicationScoped;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSignature;
import tk.labyrinth.omniboardone.tool.RepositoryBase;

import java.time.Instant;

@ApplicationScoped
public class GitlabMergeRequestSnapshotRepository implements RepositoryBase<GitlabMergeRequestSnapshot> {
	//
	// FIXME: Won't work due to object issue.
//	public List<GitlabMergeRequestSnapshot> findAllByProjectSignature(GitlabProjectSignature projectSignature) {
//		return List.ofAll(find("projectSignature", projectSignature).list());
//	}

	public List<GitlabMergeRequestSnapshot> findAllOpenByProjectSignatures(
			List<GitlabProjectSignature> projectSignatures) {
		return List.ofAll(queryOpenByProjectSignatures(projectSignatures).list());
	}

	@Nullable
	public GitlabMergeRequestSnapshot findByGitlabIid(Long iid) {
		return find("mergeRequest.iid", iid).firstResult();
	}

	@Nullable
	public Instant findLastUpdatedAt(GitlabProjectSignature projectSignature) {
		// {projectSignature: {\"baseUrl\":?1,\"id\":?2}}
		GitlabMergeRequestSnapshot result = find(
				"projectSignature.baseUrl = ?1 AND projectSignature.id = ?2",
				Sort.descending("mergeRequest.updatedAt"),
				projectSignature.getBaseUrl(), projectSignature.getId())
				.firstResult();
		//
		return result != null ? result.getMergeRequest().getUpdatedAt().toInstant() : null;
	}

	public PanacheQuery<GitlabMergeRequestSnapshot> queryByProjectSignature(GitlabProjectSignature projectSignature) {
		// FIXME
		return find("projectSignature.baseUrl = ?1 AND projectSignature.id = ?2",
				projectSignature.getBaseUrl(), projectSignature.getId());
	}

	public PanacheQuery<GitlabMergeRequestSnapshot> queryByProjectSignatures(
			List<GitlabProjectSignature> projectSignatures) {
		// FIXME: For some reason comparison with objects does not work.
		//  It may be due to codecs transforming id into _id, need to check.
		return find(
				"projectSignature.baseUrl IN ?1 AND projectSignature.id IN ?2",
				projectSignatures.map(GitlabProjectSignature::getBaseUrl).asJava(),
				projectSignatures.map(GitlabProjectSignature::getId).asJava());
//		return List.ofAll(find("projectSignature IN ?1", projectSignatures).list());
	}

	public PanacheQuery<GitlabMergeRequestSnapshot> queryOpenByProjectSignatures(
			List<GitlabProjectSignature> projectSignatures) {
		// FIXME: For some reason comparison with objects does not work.
		//  It may be due to codecs transforming id into _id, need to check.
		return find(
				"projectSignature.baseUrl IN ?1 AND projectSignature.id IN ?2 AND mergeRequest.state = opened",
				projectSignatures.map(GitlabProjectSignature::getBaseUrl).asJava(),
				projectSignatures.map(GitlabProjectSignature::getId).asJava());
//		return List.ofAll(find("projectSignature IN ?1", projectSignatures).list());
	}
}
