package tk.labyrinth.omniboardone.domain.gitlab.client;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

/**
 * <a href="https://docs.gitlab.com/ee/api/branches.html">https://docs.gitlab.com/ee/api/branches.html</a><br>
 * <a href="https://quarkus.io/guides/rest-client-reactive">https://quarkus.io/guides/rest-client-reactive</a><br>
 */
public interface GitlabBranchesApiClient {

	@GET
	@Path("/projects/{projectId}/repository/branches")
	JsonNode getBranches(String projectId);
}
