package tk.labyrinth.omniboardone.domain.pipeline;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.annotation.Nullable;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.models.PipelineStatus;
import tk.labyrinth.omniboardone.common.Hyperlink;
import tk.labyrinth.omniboardone.domain.delivery.DeliveryItem;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProject;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSignature;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSnapshot;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSnapshotRepository;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectUtils;
import tk.labyrinth.omniboardone.domain.gitlabproject.view.GitlabProjectGridView;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardRootLayout;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardWorkspaceLayout;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.domain.workspace.WorkspaceRepository;
import tk.labyrinth.omniboardone.util.RenderUtils;
import tk.labyrinth.omniboardone.util.ViewUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.WhiteSpace;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.theme.LumoBadgeVariables;

import java.util.function.Function;

@RequiredArgsConstructor
@Route(value = "pipeline/:" + PipelinePage.PIPELINE_REFERENCE_NAME, layout = OmniboardWorkspaceLayout.class)
public class PipelinePage extends CssVerticalLayout implements BeforeEnterObserver {

	public static final String PIPELINE_REFERENCE_NAME = "pipelineReference";

	private final GitlabProjectSnapshotRepository gitlabProjectSnapshotRepository;

	private final PipelineRepository pipelineRepository;

	private final WorkspaceRepository workspaceRepository;

	private void render(@NonNull Workspace workspace, @NonNull Pipeline pipeline) {
		List<GitlabProject> relevantProjects = workspace.getGitlabProjectsOrEmpty()
				.filter(gitlabProjectState -> gitlabProjectState.getUrl().contains(pipeline.getProjectFilter()));
		//
		Map<GitlabProjectSignature, GitlabProjectSnapshot> snapshotMap = gitlabProjectSnapshotRepository
				.findAllByWorkspaceUid(workspace.getUid())
				.toMap(GitlabProjectSnapshot::getSignature, Function.identity());
		//
		String commonStart = GitlabProjectUtils.computeCommonStart(relevantProjects.toList());
		//
		removeAll();
		//
		Grid<GitlabProjectGridView.Item> grid = ViewUtils.createGrid();
		{
			grid
					.addComponentColumn(item -> RenderUtils.renderUrl(
							"%s/-/pipelines".formatted(item.project().getUrl()),
							item.project().getUrl().substring(commonStart.length())))
					.setHeader("URL")
					.setResizable(true);
			//
			pipeline.getSteps().forEach(step -> grid
					.addComponentColumn(item -> {
						Component result;
						{
							GitlabProjectSnapshot projectSnapshot = item.snapshot();
							//
							if (projectSnapshot != null) {
								DeliveryItem deliveryItem = PipelineUtils.createDeliveryItem(projectSnapshot, step);
								//
								if (deliveryItem != null) {
									result = renderDeliveryItem(deliveryItem);
								} else {
									result = new Div();
								}
							} else {
								result = new Div();
							}
						}
						return result;
					})
					.setHeader(step.getName())
					.setResizable(true));
		}
		{
			grid.setItems(relevantProjects
					.map(project -> GitlabProjectGridView.Item.builder()
							.project(project)
							.snapshot(snapshotMap.get(project.getSignature()).getOrNull())
							.build())
					.asJava());
		}
		add(grid);
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		render(
				workspaceRepository.findByReference(event.getRouteParameters()
						.get(OmniboardRootLayout.WORKSPACE_REFERENCE_NAME)
						.orElseThrow()),
				pipelineRepository.findByReference(event.getRouteParameters()
						.get(PIPELINE_REFERENCE_NAME)
						.orElseThrow()));
	}

	public static Component renderDeliveryItem(DeliveryItem deliveryItem) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.setAlignItems(AlignItems.BASELINE);
			//
			StyleUtils.setCssProperty(layout, WhiteSpace.PRE);
		}
		{
			layout.addHorizontalLayout(line -> {
				line.add(renderDeploymentStatus(deliveryItem.getStatus()));
				//
				line.add(new Span(" "));
				//
				line.add(new Span(RenderUtils.format(deliveryItem.getHappenedAt())));
			});
			//
			if (deliveryItem.getRefLink() != null) {
				Hyperlink link = deliveryItem.getRefLink();
				//
				layout.addHorizontalLayout(line -> {
					line.add("Ref: ");
					//
					line.add(RenderUtils.renderHyperlink(link));
				});
			}
			//
			if (deliveryItem.getCommitLink() != null) {
				Hyperlink link = deliveryItem.getCommitLink();
				//
				layout.addHorizontalLayout(line -> {
					line.add("Commit: ");
					//
					line.add(RenderUtils.renderHyperlink(link));
				});
			}
		}
		return layout;
	}

	public static Component renderDeploymentStatus(String status) {
		Component result;
		{
			String colourTheme = switch (status) {
				case "canceled" -> LumoBadgeVariables.WARNING;
				case "failed" -> LumoBadgeVariables.ERROR;
				case "SUCCESS" -> LumoBadgeVariables.SUCCESS;
				case "running" -> "";
				default -> LumoBadgeVariables.CONTRAST;
			};
			//
			result = RenderUtils.renderBadge(status, colourTheme);
		}
		return result;
	}

	public static Component renderDeploymentStatus(Constants.DeploymentStatus deploymentStatus) {
		Component result;
		{
			String colourTheme = switch (deploymentStatus) {
				case CANCELED -> LumoBadgeVariables.WARNING;
				case FAILED -> LumoBadgeVariables.ERROR;
				case SUCCESS -> LumoBadgeVariables.SUCCESS;
				case RUNNING -> "";
				default -> LumoBadgeVariables.CONTRAST;
			};
			//
			result = RenderUtils.renderBadge(deploymentStatus.name(), colourTheme);
		}
		return result;
	}

	public static Component renderPipelineStatus(PipelineStatus pipelineStatus) {
		Component result;
		{
			String colourTheme = switch (pipelineStatus) {
				case CANCELED -> LumoBadgeVariables.WARNING;
				case FAILED -> LumoBadgeVariables.ERROR;
				case SUCCESS -> LumoBadgeVariables.SUCCESS;
				case RUNNING -> "";
				default -> LumoBadgeVariables.CONTRAST;
			};
			//
			result = RenderUtils.renderBadge(pipelineStatus.name(), colourTheme);
		}
		return result;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Item {

		@NonNull
		GitlabProject project;

		@Nullable
		GitlabProjectSnapshot state;
	}
}
