package tk.labyrinth.omniboardone.domain.gitstate;

import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.time.Instant;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class GitRevision {

	Instant createdAt;

	String hash;
}
