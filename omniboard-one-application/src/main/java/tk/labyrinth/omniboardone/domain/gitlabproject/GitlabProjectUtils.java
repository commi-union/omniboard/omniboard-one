package tk.labyrinth.omniboardone.domain.gitlabproject;

import io.vavr.collection.List;

import java.net.URI;
import java.util.Objects;

public class GitlabProjectUtils {

	private static String doComputeCommonStart(List<String> strings) {
		String result;
		{
			if (!strings.isEmpty()) {
				String firstString = strings.get(0);
				//
				int index = 0;
				while (index < firstString.length()) {
					Character firstCharacter = firstString.charAt(index);
					//
					int finalIndex = index;
					if (strings.exists(string -> !Objects.equals(string.charAt(finalIndex), firstCharacter))) {
						break;
					}
					index++;
				}
				//
				result = firstString.substring(0, index);
			} else {
				result = "";
			}
		}
		return result;
	}

	public static String computeCommonStart(List<GitlabProject> gitlabProjects) {
		return doComputeCommonStart(gitlabProjects.map(GitlabProject::getUrl));
	}

	public static String extractBaseUrl(String url) {
		return extractBaseUrl(URI.create(url));
	}

	public static String extractBaseUrl(URI uri) {
		return "%s://%s".formatted(uri.getScheme(), uri.getAuthority());
	}
}
