package tk.labyrinth.omniboardone.domain.gitlab;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.models.Deployment;

import java.time.Instant;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class GitlabDeploymentSnapshot {

	Instant createdAt;

	@Deprecated
	Deployment gitlabDeployment;

	Long gitlabId;

	String ref;

	String sha;

	Constants.DeploymentStatus status;

	Instant updatedAt;
}
