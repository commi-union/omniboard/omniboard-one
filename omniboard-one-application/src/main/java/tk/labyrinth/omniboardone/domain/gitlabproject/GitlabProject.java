package tk.labyrinth.omniboardone.domain.gitlabproject;

import jakarta.annotation.Nullable;
import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class GitlabProject {

	/**
	 * Initially we define project with url, but then we acquire id and use signature as permanent reference.<br>
	 */
	@Nullable
	GitlabProjectSignature signature;

	String url;
}
