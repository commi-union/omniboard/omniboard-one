package tk.labyrinth.omniboardone.domain.codeflow;

import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSignature;

@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EditableCodeFlow {

	String name;

	@NonNull
	List<GitlabProjectSignature> projectSignatures;

	String slug;

	@NonNull
	List<CodeFlowStage> stages;
}
