package tk.labyrinth.omniboardone.domain.codeflow.view;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.gitlab.mergerequest.GitlabMergeRequestSnapshot;
import tk.labyrinth.omniboardone.domain.gitlab.mergerequest.GitlabMergeRequestSnapshotRepository;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSignature;

import java.util.Comparator;
import java.util.stream.Stream;

@ApplicationScoped
@RequiredArgsConstructor
public class CodeFlowHandler {

	private final GitlabMergeRequestSnapshotRepository gitlabMergeRequestSnapshotRepository;

	public Map<GitlabProjectSignature, List<GitlabMergeRequestSnapshot>> fetchMergeRequests(
			List<GitlabProjectSignature> projectSignatures, boolean openOnly) {
		Map<GitlabProjectSignature, List<GitlabMergeRequestSnapshot>> result;
		{
			if (openOnly) {
				result = gitlabMergeRequestSnapshotRepository
						.findAllOpenByProjectSignatures(projectSignatures)
						.groupBy(GitlabMergeRequestSnapshot::getProjectSignature);
			} else {
				result = Stream.<GitlabMergeRequestSnapshot>concat(
								projectSignatures.toJavaStream().flatMap(projectSignature -> gitlabMergeRequestSnapshotRepository
										.queryByProjectSignature(projectSignature)
										.page(0, 5)
										.firstPage()
										.stream()),
								gitlabMergeRequestSnapshotRepository.queryOpenByProjectSignatures(projectSignatures).stream())
						.collect(List.collector())
						.sorted(Comparator
								.<GitlabMergeRequestSnapshot, Long>comparing(snapshot -> snapshot.getMergeRequest().getIid())
								.reversed())
						.groupBy(GitlabMergeRequestSnapshot::getProjectSignature)
						.mapValues(List::distinct);
			}
		}
		return result;
	}
}
