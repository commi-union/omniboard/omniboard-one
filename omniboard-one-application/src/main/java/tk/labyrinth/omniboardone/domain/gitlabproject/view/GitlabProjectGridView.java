package tk.labyrinth.omniboardone.domain.gitlabproject.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.annotation.Nullable;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.Dependent;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProject;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSignature;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSnapshot;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSnapshotRepository;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectWorker;
import tk.labyrinth.omniboardone.domain.gitstate.GitBranchSnapshot;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.util.RenderUtils;
import tk.labyrinth.omniboardone.util.ViewUtils;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.GridUtils;

import java.util.function.Function;

@Dependent
@RequiredArgsConstructor
public class GitlabProjectGridView implements View {

	private final GitlabProjectSnapshotRepository gitlabProjectSnapshotRepository;

	private final GitlabProjectWorker gitlabProjectWorker;

	private final Grid<Item> grid = ViewUtils.createGrid();

	// FIXME: Should not be a field.
	private Parameters currentParameters;

	@PostConstruct
	private void postConstruct() {
		{
			grid
					.addComponentColumn(item -> RenderUtils.renderUrl(item.project().getUrl()))
					.setFlexGrow(2)
					.setHeader("URL")
					.setResizable(true);
			grid
					.addColumn(item -> item.project().getSignature() != null
							? item.project().getSignature().getId()
							: null)
					.setFlexGrow(1)
					.setHeader("Project Id")
					.setResizable(true);
			grid
					.addComponentColumn(item -> item.snapshot() != null
							? GridUtils.renderValueList(
							item.snapshot().getBranches()
									.take(2)
									.map(GitBranchSnapshot::getName))
							: new Div())
					.setFlexGrow(2)
					.setHeader("Branches")
					.setResizable(true);
			grid
					.addComponentColumn(item -> item.snapshot() != null
							? GridUtils.renderValueList(
							item.snapshot().getPipelines()
									.take(2)
									.map(pipeline -> "%s %s".formatted(pipeline.getRef(), pipeline.getSource())))
							: new Div())
					.setFlexGrow(2)
					.setHeader("Pipelines")
					.setResizable(true);
			grid
					.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
							.icon(VaadinIcon.REFRESH.create())
							.onClick(event -> {
								GitlabProject project = item.project();
								//
								if (project.getSignature() != null) {
									gitlabProjectWorker.refreshProjectSnapshot(currentParameters.workspace().getUid(), project);
								} else {
									gitlabProjectWorker.discoverProject(currentParameters.workspace(), project.getUrl());
								}
								//
								UI.getCurrent().getPage().reload();
							})
							.themeVariants(ButtonVariant.LUMO_SMALL)
							.build()))
					.setFlexGrow(0);
		}
	}

	@Override
	public Component asVaadinComponent() {
		return grid;
	}

	public void render(Parameters parameters) {
		this.currentParameters = parameters;
		//
		Map<GitlabProjectSignature, GitlabProjectSnapshot> snapshotMap = gitlabProjectSnapshotRepository
				.findAllByWorkspaceUid(parameters.workspace().getUid())
				.toMap(GitlabProjectSnapshot::getSignature, Function.identity());
		//
		grid.setItems(parameters.projects()
				.map(project -> Item.builder()
						.project(project)
						.snapshot(snapshotMap.get(project.getSignature()).getOrNull())
						.build())
				.asJava());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		List<GitlabProject> projects;

		// FIXME
		@NonNull
		Workspace workspace;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Item {

		@NonNull
		GitlabProject project;

		@Nullable
		GitlabProjectSnapshot snapshot;
	}
}
