package tk.labyrinth.omniboardone.domain.pipeline;

import jakarta.annotation.Nullable;
import tk.labyrinth.omniboardone.common.Hyperlink;
import tk.labyrinth.omniboardone.domain.delivery.DeliveryItem;
import tk.labyrinth.omniboardone.domain.gitlab.GitlabDeploymentSnapshot;
import tk.labyrinth.omniboardone.domain.gitlab.GitlabEnvironmentSnapshot;
import tk.labyrinth.omniboardone.domain.gitlab.GitlabPipelineSnapshot;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSnapshot;

import java.util.Objects;

public class PipelineUtils {

	@Nullable
	public static DeliveryItem createDeliveryItem(GitlabProjectSnapshot projectSnapshot, PipelineStep pipelineStep) {
		DeliveryItem result;
		{
			GitlabEnvironmentSnapshot environmentSnapshot = projectSnapshot.getEnvironments()
					.filter(environmentSnapshotElement -> pipelineStep.getFilters().exists(filter ->
							Objects.equals(filter, environmentSnapshotElement.getSlug())))
					.getOrNull();
			//
			if (environmentSnapshot != null) {
				GitlabDeploymentSnapshot deploymentSnapshot = projectSnapshot.getDeployments()
						.filter(deployment -> Objects.equals(
								deployment.getGitlabId(),
								environmentSnapshot.getLastDeploymentGitlabId()))
						.get();
				//
				result = DeliveryItem.builder()
						.commitLink(Hyperlink.of(
								"%s/-/commit/%s".formatted(projectSnapshot.getUrl(), deploymentSnapshot.getSha()),
								deploymentSnapshot.getSha().substring(0, 8)))
						.happenedAt(deploymentSnapshot.getUpdatedAt())
						.refLink(Hyperlink.of(
								"%s/-/tree/%s".formatted(projectSnapshot.getUrl(), deploymentSnapshot.getRef()),
								deploymentSnapshot.getRef()))
						.status(deploymentSnapshot.getStatus().name())
						.text(null)
						.build();
			} else {
				GitlabPipelineSnapshot pipelineSnapshot = projectSnapshot.getPipelines()
						.filter(pipelineSnapshotElement -> pipelineStep.getFilters().exists(filter ->
								Objects.equals(filter, pipelineSnapshotElement.getRef())))
						.getOrNull();
				//
				if (pipelineSnapshot != null) {
					result = DeliveryItem.builder()
							.commitLink(pipelineSnapshot.getSha() != null
									? Hyperlink.of(
									"%s/-/commit/%s".formatted(projectSnapshot.getUrl(), pipelineSnapshot.getSha()),
									pipelineSnapshot.getSha().substring(0, 8))
									: null)
							.happenedAt(pipelineSnapshot.getUpdatedAt())
							.refLink(Hyperlink.of(
									"%s/-/tree/%s".formatted(projectSnapshot.getUrl(), pipelineSnapshot.getRef()),
									pipelineSnapshot.getRef()))
							.status(pipelineSnapshot.getStatus().name())
							.text(null)
							.build();
				} else {
					result = null;
				}
			}
		}
		return result;
	}
}
