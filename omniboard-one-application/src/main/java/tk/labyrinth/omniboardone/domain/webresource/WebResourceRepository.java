package tk.labyrinth.omniboardone.domain.webresource;

import jakarta.annotation.Nullable;
import jakarta.enterprise.context.ApplicationScoped;
import tk.labyrinth.omniboardone.tool.RepositoryBase;

import java.util.UUID;

@ApplicationScoped
public class WebResourceRepository implements RepositoryBase<WebResource> {

	@Nullable
	public WebResource findByUid(UUID uid) {
		return find("_id", uid).firstResult();
	}

	@Nullable
	public WebResource findByUrl(String url) {
		return find("url", url).firstResult();
	}
}
