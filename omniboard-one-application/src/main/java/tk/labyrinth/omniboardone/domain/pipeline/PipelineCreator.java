package tk.labyrinth.omniboardone.domain.pipeline;

import io.quarkus.runtime.Startup;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.domain.workspace.WorkspaceRepository;

import java.util.UUID;

@ApplicationScoped
@RequiredArgsConstructor
@Startup
public class PipelineCreator {

	private final PipelineRepository pipelineRepository;

	private final WorkspaceRepository workspaceRepository;

	@PostConstruct
	public void postConstruct() {
		List<Workspace> workspaces = workspaceRepository.findAllAsList();
		//
		UUID workspaceUid = UUID.fromString("d350c5e0-78d4-4218-950f-39058c126f6d");
		//
		pipelineRepository.persistOrUpdate(List.of(
				Pipeline.builder()
						.name("Pandora")
						.projectFilter("https://gitlab.com/commi-")
						.slug("pandora")
						.steps(List.of(
								PipelineStep.builder()
										.name("dev")
										.filters(List.of("dev"))
										.build()))
						.uid(UUID.fromString("4ac5db57-3278-4ee1-888f-1c5faf51b086"))
						.workspaceUid(workspaceUid)
						.build(),
				Pipeline.builder()
						.name("STLM FEs")
						.projectFilter("https://gitbud.epam.com")
						.slug("stlm-fes")
						.steps(List.of(
								PipelineStep.builder()
										.name("development")
										.filters(List.of("development"))
										.build(),
								PipelineStep.builder()
										.name("staging")
										.filters(List.of("staging"))
										.build(),
								PipelineStep.builder()
										.name("beta")
										.filters(List.of("beta"))
										.build()))
						.uid(UUID.fromString("ab157e1d-e526-456f-934a-bc7fcdeecc37"))
						.workspaceUid(workspaceUid)
						.build()));
	}
}
