package tk.labyrinth.omniboardone.domain.workspace;

import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import lombok.experimental.FieldDefaults;
import org.bson.codecs.pojo.annotations.BsonId;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProject;

import java.util.UUID;

@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@With
public class Workspace {

	List<String> environments;

	List<GitlabProject> gitlabProjects;

	String name;

	@BsonId
	UUID uid;

	public String computeReference() {
		return name != null ? name : uid.toString();
	}

	public List<GitlabProject> getGitlabProjectsOrEmpty() {
		return gitlabProjects != null ? gitlabProjects : List.of();
	}
}
