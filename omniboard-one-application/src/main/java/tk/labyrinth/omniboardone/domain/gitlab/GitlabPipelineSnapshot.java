package tk.labyrinth.omniboardone.domain.gitlab;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.gitlab4j.api.models.Pipeline;
import org.gitlab4j.api.models.PipelineStatus;

import java.time.Instant;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class GitlabPipelineSnapshot {

	Instant createdAt;

	Long gitlabId;

	@Deprecated
	Pipeline gitlabPipeline;

	String ref;

	String sha;

	String source;

	PipelineStatus status;

	Instant updatedAt;
}
