package tk.labyrinth.omniboardone.domain.gitlab.client;

import com.fasterxml.jackson.databind.JsonNode;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;

/**
 * <a href="https://docs.gitlab.com/ee/api/projects.html">https://docs.gitlab.com/ee/api/projects.html</a>
 * <a href="https://quarkus.io/guides/rest-client-reactive">https://quarkus.io/guides/rest-client-reactive</a><br>
 */
@Path("api/v4")
public interface GitlabProjectsApiClient {
//		GitlabProjectsApiClient projectsApiClient = QuarkusRestClientBuilder.newBuilder()
//				.baseUri(URI.create(gitlabBaseUrl))
//				.build(GitlabProjectsApiClient.class);

	@GET
	@Path("projects/{projectPath}")
	JsonNode getProject(@PathParam("projectPath") String projectPath);

	@GET
	@Path("projects")
	JsonNode getProjects(@QueryParam("search") String search);
}
