package tk.labyrinth.omniboardone.domain.codeflow;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class CodeFlowStage {

	String inboundRefFilter;

	String name;
}
