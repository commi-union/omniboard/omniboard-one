package tk.labyrinth.omniboardone.domain.sidebar;

import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;

@ParentLayout(OmniboardRootLayout.class)
@RequiredArgsConstructor
@RoutePrefix("workspace/:" + OmniboardRootLayout.WORKSPACE_REFERENCE_NAME)
public class OmniboardWorkspaceLayout extends CssGridLayout implements RouterLayout {
	// We use grid so that content takes all the space within.
}
