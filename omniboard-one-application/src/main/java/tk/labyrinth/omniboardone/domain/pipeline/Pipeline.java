package tk.labyrinth.omniboardone.domain.pipeline;

import io.vavr.collection.List;
import jakarta.annotation.Nullable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import lombok.experimental.FieldDefaults;
import org.bson.codecs.pojo.annotations.BsonId;

import java.util.UUID;

@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@With
public class Pipeline {

	String name;

	@Nullable
	String projectFilter;

	String slug;

	List<PipelineStep> steps;

	@BsonId
	UUID uid;

	UUID workspaceUid;
}
