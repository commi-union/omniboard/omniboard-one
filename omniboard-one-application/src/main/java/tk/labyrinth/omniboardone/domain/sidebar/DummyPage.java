package tk.labyrinth.omniboardone.domain.sidebar;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;

@Route(value = "dummy", layout = OmniboardWorkspaceLayout.class)
public class DummyPage extends Div {

	@PostConstruct
	private void postConstruct() {
		add(new Span("DUMMY"));
	}
}
