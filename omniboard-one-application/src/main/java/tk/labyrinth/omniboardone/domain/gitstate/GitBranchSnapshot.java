package tk.labyrinth.omniboardone.domain.gitstate;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class GitBranchSnapshot {

	@NonNull
	GitRevision head;

	@NonNull
	String name;
}
