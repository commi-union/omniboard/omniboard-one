package tk.labyrinth.omniboardone.domain.gitlabproject;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class GitlabProjectSignature {

	@NonNull
	String baseUrl;

	@NonNull
	Long id;
}
