package tk.labyrinth.omniboardone.domain.pipeline;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class PipelineStep {

	List<String> filters;

	String name;
}
