package tk.labyrinth.omniboardone.domain.gitlabproject;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EditableGitlabProject {

	String url;
}
