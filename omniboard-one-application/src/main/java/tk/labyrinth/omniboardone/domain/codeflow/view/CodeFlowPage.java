package tk.labyrinth.omniboardone.domain.codeflow.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Try;
import jakarta.annotation.Nullable;
import lombok.RequiredArgsConstructor;
import org.gitlab4j.api.models.ApprovalRule;
import org.gitlab4j.api.models.ApprovalState;
import org.gitlab4j.api.models.MergeRequest;
import tk.labyrinth.omniboardone.domain.codeflow.CodeFlow;
import tk.labyrinth.omniboardone.domain.codeflow.CodeFlowRepository;
import tk.labyrinth.omniboardone.domain.gitlab.mergerequest.GitlabMergeRequestSnapshot;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProject;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSignature;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectUtils;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectWorker;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardRootLayout;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardWorkspaceLayout;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.domain.workspace.WorkspaceRepository;
import tk.labyrinth.omniboardone.iam.AccessManager;
import tk.labyrinth.omniboardone.util.RenderUtils;
import tk.labyrinth.omniboardone.util.ViewUtils;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponent2;
import tk.labyrinth.pandora.functionalvaadin.component.AvatarRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.RadioButtonGroupRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.SpanRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.fault.FaultViews;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.theme.LumoBadgeVariables;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.Objects;

@RequiredArgsConstructor
@Route(value = "codeflow/:" + CodeFlowPage.CODE_FLOW_REFERENCE_NAME, layout = OmniboardWorkspaceLayout.class)
public class CodeFlowPage extends Div implements BeforeEnterObserver {

	public static final String CODE_FLOW_REFERENCE_NAME = "codeFlowReference";

	private final AccessManager accessManager;

	private final CodeFlowHandler codeFlowHandler;

	private final CodeFlowRepository codeFlowRepository;

	private final GitlabProjectWorker gitlabProjectWorker;

	private final WorkspaceRepository workspaceRepository;

	private Component render(Workspace workspace, CodeFlow codeFlow) {
		boolean isAdmin = accessManager.isCurrentUserAdmin();
		//
		List<GitlabProject> projects = workspace.getGitlabProjectsOrEmpty()
				.filter(gitlabProject -> codeFlow.getProjectSignatures().contains(gitlabProject.getSignature()));
		//
		String commonStart = GitlabProjectUtils.computeCommonStart(projects);
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
			//
			layout.setHeightFull();
		}
		{
			Observable<String> filterObservable = Observable.withInitialValue("Open");
			{
				layout.add(FunctionalComponent2.of(
						filterObservable,
						(state, sink) -> {
							CssHorizontalLayout toolbar = new CssHorizontalLayout();
							{
								toolbar.addClassName(PandoraStyles.LAYOUT);
							}
							{
								toolbar.add(RadioButtonGroupRenderer.<String>render(builder -> builder
										.items(List.of("Open", "All").asJava())
										.onValueChange(event -> sink.accept(event.getValue()))
										.value(state)
										.build()));
							}
							return toolbar;
						}));
			}
			{
				Grid<GitlabProject> grid = ViewUtils.createGrid();
				{
					grid.setDetailsVisibleOnClick(false);
					grid.setSelectionMode(Grid.SelectionMode.NONE);
				}
				{
					grid
							.addComponentColumn(item -> RenderUtils.renderUrl(
									item.getUrl(),
									item.getUrl().substring(commonStart.length())))
							.setHeader("Project")
							.setResizable(true);
					//
					// FIXME: Rework into calling Function.
//					grid
//							.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
//									.enabled(isAdmin)
//									.onClick(event -> Try.ofSupplier(() -> {
//												// TODO: Use Function.
//												gitlabProjectWorker.fetchMergeRequests(item.getSignature(), true);
//												//
//												return null;
//											})
//											.onSuccess(v -> UI.getCurrent().getPage().reload())
//											.onFailure(FaultViews::showDialog))
//									.text("Fetch")
//									.themeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL)
//									.build()))
//							.setFlexGrow(0);
				}
				{
					Observable<Map<GitlabProjectSignature, List<GitlabMergeRequestSnapshot>>> signatureToMergeRequestsMapObservable =
							filterObservable.map(nextFilter -> codeFlowHandler.fetchMergeRequests(
									projects.map(GitlabProject::getSignature),
									Objects.equals(filterObservable.get(), "Open")));
					//
					grid.setItemDetailsRenderer(new ComponentRenderer<>(item -> {
						CssGridLayout mergeRequestsLayout = new CssGridLayout();
						{
							mergeRequestsLayout.setGridTemplateColumns("repeat(3,1fr)");
							//
							mergeRequestsLayout.addClassName(PandoraStyles.LAYOUT_SMALL);
						}
						{
							List<GitlabMergeRequestSnapshot> mergeRequestSnapshots = signatureToMergeRequestsMapObservable.get()
									.get(item.getSignature())
									.getOrElse(List.empty());
							//
							mergeRequestSnapshots.forEach(mergeRequestSnapshot -> mergeRequestsLayout.add(renderMergeRequestCard(
									isAdmin,
									item.getSignature(),
									mergeRequestSnapshot.getMergeRequest(),
									mergeRequestSnapshot.getApprovalState())));
						}
						return mergeRequestsLayout;
					}));
				}
				{
					grid.setItems(projects.asJava());
					projects.forEach(project -> grid.setDetailsVisible(project, true));
				}
				{
					filterObservable.subscribe(next -> grid.getDataProvider().refreshAll());
				}
				layout.add(grid);
			}
		}
		return layout;
	}

	private Component renderMergeRequestCard(
			boolean isAdmin,
			GitlabProjectSignature projectSignature,
			MergeRequest mergeRequest,
			@Nullable ApprovalState approvalState) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.CARD, PandoraStyles.LAYOUT_SMALL);
		}
		{
			layout.addHorizontalLayout(headerLayout -> {
				{
					headerLayout.addClassName(PandoraStyles.LAYOUT_SMALL);
				}
				{
					headerLayout.add(RenderUtils.renderUrl(mergeRequest.getWebUrl(), "#%s".formatted(mergeRequest.getIid())));
					//
					headerLayout.add(renderMergeRequestState(mergeRequest.getState()));
					//
					headerLayout.add(renderPipelineStatus(mergeRequest.getHeadPipeline() != null
							? mergeRequest.getHeadPipeline().getStatus().toValue()
							: null));
					//
					headerLayout.addStretch();
					//
					// TODO: Move into expanded view.
//					headerLayout.add(ButtonRenderer.render(builder -> builder
//							.enabled(isAdmin)
//							.onClick(event -> Try.ofSupplier(() -> {
//										gitlabProjectWorker.fetchMergeRequest(projectSignature, mergeRequest.getIid());
//										//
//										return null;
//									})
//									.onSuccess(v -> UI.getCurrent().getPage().reload())
//									.onFailure(FaultViews::showDialog))
//							.text("Fetch")
//							.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
//							.build()));
				}
			});
			//
			layout.add();
			//
			layout.add(mergeRequest.getTitle());
			//
			layout.add(new Span("%s -> %s".formatted(mergeRequest.getSourceBranch(), mergeRequest.getTargetBranch())));
			//
//			layout.add(new Span("Created at %s by %s".formatted(
//					RenderUtils.format(mergeRequest.getCreatedAt()),
//					mergeRequest.getAuthor().getName())));
//			//
//			layout.add(new Span("Updated at %s".formatted(
//					RenderUtils.format(mergeRequest.getUpdatedAt()))));
			//
			layout.addStretch();
			//
			layout.addHorizontalLayout(peopleLayout -> {
				{
					peopleLayout.addClassName(PandoraStyles.LAYOUT_SMALL);
				}
				{
					peopleLayout.addVerticalLayout(authorLayout -> {
						// TODO: Add label for Author and other groups of people.
						//
						authorLayout.add(AvatarRenderer.render(builder -> builder
								.imageUrl(mergeRequest.getAuthor().getAvatarUrl())
								.name("Authored by %s".formatted(mergeRequest.getAuthor().getName()))
								.tooltipEnabled(true)
								.build()));
					});
					//
					{
						CssHorizontalLayout approversLayout = new CssHorizontalLayout();
						{
							CssFlexItem.setFlexGrow(approversLayout, 1);
						}
						{
							if (approvalState != null) {
								List.ofAll(approvalState.getRules())
										.flatMap(ApprovalRule::getApprovedBy)
										.forEach(approver ->
												approversLayout.add(AvatarRenderer.render(builder -> builder
														.imageUrl(approver.getAvatarUrl())
														.name(approver.getName())
														.tooltipEnabled(true)
														.build())));
							}
						}
						peopleLayout.add(approversLayout);
					}
					//
					if (mergeRequest.getMergeUser() != null) {
						peopleLayout.add(AvatarRenderer.render(builder -> builder
								.imageUrl(mergeRequest.getMergeUser().getAvatarUrl())
								.name("Merged by %s".formatted(mergeRequest.getMergeUser().getName()))
								.tooltipEnabled(true)
								.build()));
					}
				}
			});
		}
		return layout;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		Component component = Try.ofSupplier(() -> {
					CodeFlow codeFlow = codeFlowRepository.findByReference(event
							.getRouteParameters()
							.get(CODE_FLOW_REFERENCE_NAME)
							.orElse(null));
					Workspace workspace = workspaceRepository.findByReference(event
							.getRouteParameters()
							.get(OmniboardRootLayout.WORKSPACE_REFERENCE_NAME)
							.orElse(null));
					//
					return render(workspace, codeFlow);
				})
				.recover(FaultViews::createView)
				.get();
		//
		removeAll();
		//
		add(component);
	}

	public static Component renderMergeRequestState(String state) {
		List<String> themes = switch (state) {
			case "closed", "merged" -> List.of(LumoBadgeVariables.CONTRAST);
			case "opened" -> List.of(LumoBadgeVariables.SUCCESS);
			default -> List.of(LumoBadgeVariables.ERROR);
		};
		//
		return SpanRenderer.render(builder -> builder
				.text(state)
				.themes(List.of(LumoBadgeVariables.BADGE, LumoBadgeVariables.PILL).appendAll(themes))
				.title("Merge Request Status")
				.build());
	}

	public static Component renderPipelineStatus(@Nullable String status) {
		List<String> themes = status != null
				? switch (status) {
			case "success" -> List.of(LumoBadgeVariables.SUCCESS);
			default -> List.of(LumoBadgeVariables.ERROR);
		}
				: List.of(LumoBadgeVariables.CONTRAST);
		//
		return SpanRenderer.render(builder -> builder
				.text(status != null ? status : "null")
				.themes(List.of(LumoBadgeVariables.BADGE, LumoBadgeVariables.PILL).appendAll(themes))
				.title("Pipeline Status")
				.build());
	}
}
