package tk.labyrinth.omniboardone.domain.codeflow.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.codeflow.CodeFlow;
import tk.labyrinth.omniboardone.domain.codeflow.CodeFlowRepository;
import tk.labyrinth.omniboardone.domain.codeflow.CodeFlowStage;
import tk.labyrinth.omniboardone.domain.codeflow.EditableCodeFlow;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProject;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSignature;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardRootLayout;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardWorkspaceLayout;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.domain.workspace.WorkspaceRepository;
import tk.labyrinth.omniboardone.iam.AccessManager;
import tk.labyrinth.omniboardone.util.RenderUtils;
import tk.labyrinth.omniboardone.util.ViewUtils;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.functionalvaadin.view.fault.FaultViews;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.util.GridUtils;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;

@RequiredArgsConstructor
@Route(value = "codeflows", layout = OmniboardWorkspaceLayout.class)
public class CodeFlowsPage extends Div implements BeforeEnterObserver {

	private final AccessManager accessManager;

	private final CodeFlowRepository codeFlowRepository;

	private final WorkspaceRepository workspaceRepository;

	private Component render(Workspace workspace) {
		boolean isAdmin = accessManager.isCurrentUserAdmin();
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			{
				addClassNames(PandoraStyles.LAYOUT);
			}
			{
				layout.addHorizontalLayout(toolbar -> {
					toolbar.addStretch();
					//
					toolbar.add(ButtonRenderer.render(builder -> builder
							.enabled(isAdmin)
							.onClick(event -> showEditDialog(workspace, CodeFlow.builder().build()))
							.text("Add")
							.themeVariants(ButtonVariant.LUMO_PRIMARY)
							.build()));
				});
				//
				{
					Map<GitlabProjectSignature, GitlabProject> signatureToProjectMap = workspace.getGitlabProjectsOrEmpty()
							.toMap(GitlabProject::getSignature, Function.identity());
					//
					List<CodeFlow> codeFlows = codeFlowRepository.findAllAsList();
					//
					Grid<CodeFlow> grid = ViewUtils.createGrid();
					{
						grid
								.addColumn(CodeFlow::getName)
								.setHeader("Name")
								.setResizable(true);
						grid
								.addColumn(CodeFlow::getSlug)
								.setHeader("Slug")
								.setResizable(true);
						grid
								.addComponentColumn(item -> GridUtils.renderComponentList(
										item.getProjectSignatures(),
										projectSignature -> RenderUtils.renderUrl(
												// TODO: The link may not align with text if project is moved and updated here. Is it good or bad for us?
												"%s/projects/%s".formatted(projectSignature.getBaseUrl(), projectSignature.getId()),
												//
												signatureToProjectMap.get(projectSignature).get().getUrl())))
								.setHeader("Projects")
								.setResizable(true);
						grid
								.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
										.icon(VaadinIcon.EDIT.create())
										.onClick(event -> showEditDialog(workspace, item))
										.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
										.build()))
								.setFlexGrow(0)
								.setResizable(false);
					}
					{
						grid.setItems(codeFlows.asJava());
					}
					layout.add(grid);
				}
			}
		}
		return layout;
	}

	private void showEditDialog(Workspace workspace, CodeFlow object) {
		boolean isAdmin = accessManager.isCurrentUserAdmin();
		//
		Binder<EditableCodeFlow> binder = new Binder<>();
		binder.setBean(EditableCodeFlow.builder()
				.name(object.getName())
				.projectSignatures(object.getProjectSignatures() != null ? object.getProjectSignatures() : List.empty())
				.slug(object.getSlug())
				.stages(object.getStages() != null ? object.getStages() : List.empty())
				.build());
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			{
				layout.add(new H3("%s CodeFlow".formatted(object.getUid() != null ? "Edit" : "Create")));
			}
			{
				FormLayout formLayout = new FormLayout();
				{
					{
						TextField nameField = new TextField("Name");
						//
						binder.bind(nameField, EditableCodeFlow::getName, EditableCodeFlow::setName);
						//
						formLayout.add(nameField);
					}
					{
						TextField slugField = new TextField("Slug");
						//
						binder.bind(slugField, EditableCodeFlow::getSlug, EditableCodeFlow::setSlug);
						//
						formLayout.add(slugField);
					}
					{
//						TextField projectFilderField = new TextField("Project Filter");
//						//
//						binder.bind(
//								projectFilderField,
//								EditableCodeFlow::getProjectFilter,
//								EditableCodeFlow::setProjectFilter);
//						//
//						formLayout.add(projectFilderField);
					}
					{
						CheckboxGroup<GitlabProjectSignature> projectSignaturesCheckboxGroup = new CheckboxGroup<>("Projects");
						//
						projectSignaturesCheckboxGroup.setItemLabelGenerator(item -> workspace.getGitlabProjectsOrEmpty()
								.find(gitlabProject -> Objects.equals(gitlabProject.getSignature(), item))
								.map(GitlabProject::getUrl)
								.get());
						projectSignaturesCheckboxGroup.setItems(workspace.getGitlabProjectsOrEmpty()
								.map(GitlabProject::getSignature)
								.filter(Objects::nonNull)
								.asJava());
						//
						binder.bind(
								projectSignaturesCheckboxGroup,
								innerObject -> innerObject.getProjectSignatures().toJavaSet(),
								(innerObject, value) -> innerObject.setProjectSignatures(List.ofAll(value)));
						//
						formLayout.add(projectSignaturesCheckboxGroup);
					}
					{
						TextField stagesField = new TextField("Stages");
						//
						binder.bind(
								stagesField,
								localObject -> localObject.getStages().map(CodeFlowStage::getInboundRefFilter).mkString(","),
								(localObject, value) -> localObject.setStages(List.of(value.split(","))
										.map(String::trim)
										.map(segment -> CodeFlowStage.builder()
												.inboundRefFilter(segment)
												.name(segment)
												.build())));
						//
						formLayout.add(stagesField);
					}
				}
				layout.add(formLayout);
			}
		}
		ConfirmationViews
				.showComponentDialog(layout)
				.subscribeAlwaysAccepted(success -> {
					if (isAdmin) {
						EditableCodeFlow value = binder.getBean();
						//
						codeFlowRepository.persistOrUpdate(object.toBuilder()
								.name(value.getName())
								.projectSignatures(value.getProjectSignatures())
								.slug(value.getSlug())
								.stages(value.getStages())
								.workspaceUid(workspace.getUid())
								.uid(object.getUid() != null ? object.getUid() : UUID.randomUUID())
								.build());
						//
						// TODO: We just need to refresh the content here.
						UI.getCurrent().getPage().reload();
					}
				});
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		Component component = Try.ofSupplier(() -> {
					Workspace workspace = workspaceRepository.findByReference(event
							.getRouteParameters()
							.get(OmniboardRootLayout.WORKSPACE_REFERENCE_NAME)
							.orElse(null));
					//
					return render(workspace);
				})
				.recover(FaultViews::createView)
				.get();
		//
		removeAll();
		//
		add(component);
	}
}
