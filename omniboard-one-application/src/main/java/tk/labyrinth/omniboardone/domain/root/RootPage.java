package tk.labyrinth.omniboardone.domain.root;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.router.RouterLink;
import io.vavr.collection.List;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardRootLayout;
import tk.labyrinth.omniboardone.domain.workspace.EditableWorkspace;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.domain.workspace.WorkspacePage;
import tk.labyrinth.omniboardone.domain.workspace.WorkspaceRepository;
import tk.labyrinth.omniboardone.iam.AccessManager;
import tk.labyrinth.pandora.functionalvaadin.component.VaadinComponents;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;

import java.util.UUID;

@RequiredArgsConstructor
@Route(value = "", layout = OmniboardRootLayout.class)
public class RootPage extends VerticalLayout {

	private final AccessManager accessManager;

	private final WorkspaceRepository workspaceRepository;

	@PostConstruct
	private void postConstruct() {
		boolean isAdmin = accessManager.isCurrentUserAdmin();
		//
		List<Workspace> workspaces = workspaceRepository.findAllAsList();
		//
		{
			HorizontalLayout workspacesLayout = new HorizontalLayout();
			{
				// TODO
			}
			{
				workspaces.forEach(workspace -> workspacesLayout.add(new RouterLink(
						workspace.getName() != null ? workspace.getName() : workspace.getUid().toString(),
						WorkspacePage.class,
						new RouteParameters(OmniboardRootLayout.WORKSPACE_REFERENCE_NAME, workspace.computeReference()))));
			}
			add(workspacesLayout);
		}
		add(VaadinComponents.button(button -> {
			button.setEnabled(isAdmin);
			button.setText("Create Workspace");
			button.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
			//
			button.addClickListener(event -> {
				FormLayout layout = new FormLayout();
				Binder<EditableWorkspace> binder = new Binder<>();
				binder.setBean(new EditableWorkspace());
				{
					{
						layout.add(new H3("Create Workspace"));
					}
					{
						TextField nameField = new TextField("Name");
						//
						binder.bind(nameField, EditableWorkspace::getName, EditableWorkspace::setName);
						//
						layout.add(nameField);
					}
				}
				ConfirmationViews
						.showComponentDialog(layout)
						.subscribeAlwaysAccepted(success -> {
							EditableWorkspace value = binder.getBean();
							//
							UUID uid = UUID.randomUUID();
							//
							workspaceRepository.persist(Workspace.builder()
									.uid(uid)
									.name(value.getName())
									.build());
							//
							UI.getCurrent().navigate(
									WorkspacePage.class,
									new RouteParameters(OmniboardRootLayout.WORKSPACE_REFERENCE_NAME, uid.toString()));
						});
			});
		}));
	}
}
