package tk.labyrinth.omniboardone.domain.webresource;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import tk.labyrinth.pandora.datatypes.simple.secret.Secret;

@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EditableWebResource {

	String kind;

	String name;

	Secret token;

	String url;
}
