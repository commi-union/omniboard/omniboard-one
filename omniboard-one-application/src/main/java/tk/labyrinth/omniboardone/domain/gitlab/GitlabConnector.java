package tk.labyrinth.omniboardone.domain.gitlab;

import io.vavr.collection.List;
import jakarta.annotation.Nullable;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.apache.commons.lang3.tuple.Pair;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.ApprovalRule;
import org.gitlab4j.api.models.ApprovalState;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.Environment;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.MergeRequestFilter;
import org.gitlab4j.api.models.Pipeline;
import org.gitlab4j.api.models.Project;
import tk.labyrinth.pandora.misc4j.java.util.function.CheckedFunction;
import tk.labyrinth.pandora.misc4j.java.util.function.CheckedSupplier;

import java.time.Instant;
import java.util.Date;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class GitlabConnector {

	@Nullable
	String accessToken;

	@NonNull
	String baseUrl;

	private <T> T wrapped(CheckedFunction<GitLabApi, T, GitLabApiException> function) {
		try (GitLabApi gitLabApi = new GitLabApi(baseUrl, accessToken)) {
			return function.apply(gitLabApi);
		} catch (GitLabApiException ex) {
			throw new RuntimeException(ex);
		}
	}

	private <T> T wrapped(CheckedSupplier<T, GitLabApiException> supplier) {
		try {
			return supplier.get();
		} catch (GitLabApiException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Project findProject(String projectPath) {
		return wrapped(gitLabApi -> {
			Project project = gitLabApi.getProjectApi().getProject(projectPath);
			//
			return project;
		});
	}

	public List<Branch> getBranches(Long projectId) {
		return wrapped(gitLabApi -> {
			List<Branch> branches = List.ofAll(gitLabApi.getRepositoryApi().getBranches(projectId));
			//
			return branches;
		});
	}

	public List<Environment> getEnvironments(Long projectId) {
		return wrapped(gitLabApi -> {
			List<Environment> rawEnvironments = List.ofAll(gitLabApi.getEnvironmentsApi().getEnvironments(projectId));
			//
			List<Environment> environments = rawEnvironments.map(environment -> wrapped(() ->
					gitLabApi.getEnvironmentsApi().getEnvironment(projectId, environment.getId())));
			//
			return environments;
		});
	}

	public Pair<MergeRequest, ApprovalState> getMergeRequestAndApprovalState(Long projectId, Long mergeRequestIid) {
		return wrapped(gitLabApi -> {
			MergeRequest mergeRequest = gitLabApi.getMergeRequestApi().getMergeRequest(projectId, mergeRequestIid);
			//
			ApprovalState approvalState;
			{
				// FIXME: Approval state endpoint does not work for some Gitlab instances (due to version or grade, not sure),
				//  so this is a workaround. If we have to support different versions we would require to fetch Gitlab version
				//  and decide based on that.
				//
//				= gitLabApi.getMergeRequestApi().getApprovalState(projectId, mergeRequestIid);
				MergeRequest approvals = gitLabApi.getMergeRequestApi().getApprovals(projectId, mergeRequestIid);
				//
				ApprovalRule approvalRule = new ApprovalRule();
				approvalRule.setApprovedBy(approvals.getApprovedBy());
				//
				approvalState = new ApprovalState();
				approvalState.setRules(List.of(approvalRule).asJava());
			}
			//
			return Pair.of(mergeRequest, approvalState);
		});
	}

	public List<MergeRequest> getMergeRequests(Long projectId, @Nullable Instant updatedAfter) {
		return wrapped(gitLabApi -> {
			List<MergeRequest> rawMergeRequests = List.ofAll(gitLabApi.getMergeRequestApi()
					.getMergeRequests(
							new MergeRequestFilter()
									.withProjectId(projectId)
									.withUpdatedAfter(updatedAfter != null ? new Date(updatedAfter.toEpochMilli() + 1) : null),
							100)
					.first());
			//
//			List<MergeRequest> mergeRequests = rawMergeRequests.map(mergeRequest -> wrapped(() ->
//					gitLabApi.getMergeRequestApi().getMergeRequest(projectId, mergeRequest.getIid())));
			//
			return rawMergeRequests;
		});
	}

	public List<Pipeline> getPipelines(Long projectId) {
		return wrapped(gitLabApi -> {
			List<Pipeline> pipelines = List.ofAll(gitLabApi.getPipelineApi().getPipelines(projectId));
			//
			return pipelines;
		});
	}
}
