package tk.labyrinth.omniboardone.domain.sidebar;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.accordion.AccordionPanel;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Location;
import com.vaadin.flow.router.RouteParam;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.theme.lumo.Lumo;
import io.vavr.collection.List;
import io.vavr.control.Try;
import jakarta.annotation.PostConstruct;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.omniboardone.domain.codeflow.CodeFlow;
import tk.labyrinth.omniboardone.domain.codeflow.CodeFlowRepository;
import tk.labyrinth.omniboardone.domain.codeflow.view.CodeFlowPage;
import tk.labyrinth.omniboardone.domain.codeflow.view.CodeFlowsPage;
import tk.labyrinth.omniboardone.domain.pipeline.PipelineRepository;
import tk.labyrinth.omniboardone.domain.root.RootPage;
import tk.labyrinth.omniboardone.domain.webresource.RootWebResourcesPage;
import tk.labyrinth.omniboardone.domain.webresource.WorkspaceWebResourcesPage;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.domain.workspace.WorkspacePage;
import tk.labyrinth.omniboardone.domain.workspace.WorkspaceRepository;
import tk.labyrinth.pandora.functionalvaadin.component.AvatarRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.functionalvaadin.view.fault.FaultViews;
import tk.labyrinth.pandora.functions.domain.function.FunctionsPage;
import tk.labyrinth.pandora.functions.domain.functionrun.FunctionRunsPage;
import tk.labyrinth.pandora.functions.domain.routine.RoutinesPage;
import tk.labyrinth.pandora.iam.domain.login.PandoraLoginViews;
import tk.labyrinth.pandora.iam.domain.useraccount.RootUserAccountsPage;
import tk.labyrinth.pandora.iam.domain.useraccount.WorkspaceUserAccountsPage;
import tk.labyrinth.pandora.iam.domain.userstate.UserAuthenticationInformation;
import tk.labyrinth.pandora.iam.domain.userstate.UserState;
import tk.labyrinth.pandora.iam.domain.userstate.UserStateHandler;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@RequiredArgsConstructor
public class OmniboardRootLayout extends CssGridLayout implements BeforeEnterObserver, RouterLayout {

	public static final String WORKSPACE_REFERENCE_NAME = "workspaceReference";

	private final CodeFlowRepository codeFlowRepository;

	private final PandoraLoginViews pandoraLoginViews;

	private final PipelineRepository pipelineRepository;

	private final Observable<State> stateObservable = Observable.notInitialized();

	private final UserStateHandler userStateHandler;

	private final WorkspaceRepository workspaceRepository;

	@PostConstruct
	private void postConstruct() {
		{
			addClassNames("pandora-root");
			//
			setGridTemplateAreas(
					"%1$s %1$s".formatted("navbar"),
					"%s %s".formatted("sidebar", "content"));
			setGridTemplateColumns("auto 1fr");
			setGridTemplateRows("auto 1fr");
			//
			setHeight("100vh");
		}
		{
			stateObservable.subscribe(next -> {
				{
					// TODO: navbar?
				}
				{
					Component sidebar = Try.ofSupplier(() -> {
								Workspace workspace = next.workspaceReference() != null
										? workspaceRepository.findByReference(next.workspaceReference())
										: null;
								//
								return renderSidebar(workspace, next.currentLocation());
							})
							.recover(FaultViews::createView)
							.get();
					//
					sidebar.addClassName("sidebar");
					//
					getChildren()
							.filter(child -> child.getClassNames().contains("sidebar"))
							.forEach(Component::removeFromParent);
					//
					add(sidebar, "sidebar");
				}
			});
		}
	}

	private Component renderSidebar(@Nullable Workspace workspace, Location currentLocation) {
		CssVerticalLayout sidebar = new CssVerticalLayout();
		//
		{
			SmartLocationRenderer locationRenderer = SmartLocationRenderer.of(currentLocation.getPathWithQueryParameters());
			//
			{
				sidebar.getElement().getThemeList().add(Lumo.DARK);
				//
				sidebar.setMaxWidth("12em");
				sidebar.setMinWidth("12em");
			}
			{
				{
					CssHorizontalLayout navbar = new CssHorizontalLayout();
					{
						navbar.addClassName(PandoraStyles.LAYOUT_SMALL);
						//
						navbar.setAlignItems(AlignItems.BASELINE);
					}
					{
						navbar.add(new Span("LOGO"));
						//
						navbar.addStretch();
						//
						{
							UserAuthenticationInformation authenticationInformation = userStateHandler.getUserState()
									.getAuthenticationInformation();
							//
							Button avatarButton = new Button();
							{
								avatarButton.addThemeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY);
							}
							{
								avatarButton.setIcon(AvatarRenderer.render(builder -> builder
										.name(authenticationInformation != null ? authenticationInformation.getDisplayName() : "?")
										.tooltipEnabled(true)
										.build()));
							}
							{
								avatarButton.addClickListener(event -> {
									if (authenticationInformation == null) {
										pandoraLoginViews.showLoginDialog();
									} else {
										ConfirmationViews
												.showTextDialog("Sign Out?")
												.subscribeAlwaysAccepted(success -> {
													userStateHandler.setUserState(UserState.builder()
															.authenticationInformation(null)
															.build());
													//
													// FIXME: Make it smooth.
													UI.getCurrent().getPage().reload();
												});
									}
								});
							}
							navbar.add(avatarButton);
						}
					}
					sidebar.add(navbar);
				}
				{
					sidebar.add(locationRenderer.renderLocation(builder -> builder
							.route(RootPage.class)
							.text("Home")
							.build()));
				}
				{
					if (workspace != null) {
						sidebar.add(locationRenderer.renderLocation(builder -> builder
								.route(WorkspacePage.class)
								.routeParameters(new RouteParameters(
										WORKSPACE_REFERENCE_NAME,
										workspace.getName() != null ? workspace.getName() : workspace.computeReference()))
								.text("Workspace")
								.build()));
					}
				}
				{
					if (workspace != null) {
						List<CodeFlow> codeFlows = codeFlowRepository.findAllByWorkspaceUid(workspace.getUid());
						//
						Accordion accordion = new Accordion();
						{
							CssVerticalLayout codeFlowsLayout = new CssVerticalLayout();
							{
								codeFlows.forEach(codeFlow -> codeFlowsLayout.add(locationRenderer.renderLocation(builder -> builder
										.route(CodeFlowPage.class)
										.routeParameters(new RouteParameters(
												new RouteParam(WORKSPACE_REFERENCE_NAME, workspace.computeReference()),
												new RouteParam(
														CodeFlowPage.CODE_FLOW_REFERENCE_NAME,
														codeFlow.getSlug() != null ? codeFlow.getSlug() : codeFlow.getUid().toString())))
										.text(codeFlow.getName())
										.build())));
							}
							{
								codeFlowsLayout.add(locationRenderer.renderLocation(builder -> builder
										.route(CodeFlowsPage.class)
										.routeParameters(new RouteParameters(WORKSPACE_REFERENCE_NAME, workspace.computeReference()))
										.text("Configure")
										.build()));
							}
							accordion.add(new AccordionPanel("Code Flows", codeFlowsLayout));
						}
						sidebar.add(accordion);
					}
					{
//						List<Pipeline> pipelines = pipelineRepository.findAllByWorkspaceUid(workspace.getUid());
						//
//						CssVerticalLayout pipelinesLayout = new CssVerticalLayout();
//						{
//							pipelines.forEach(pipeline -> pipelinesLayout.add(locationRenderer.renderLocation(builder -> builder
//									.route(PipelinePage.class)
//									.routeParameters(new RouteParameters(
//											new RouteParam(WORKSPACE_REFERENCE_NAME, workspace.computeReference()),
//											new RouteParam(
//													PipelinePage.PIPELINE_REFERENCE_NAME,
//													pipeline.getSlug() != null ? pipeline.getSlug() : pipeline.getUid().toString())))
//									.text(pipeline.getName())
//									.build())));
//						}
//						{
//							pipelinesLayout.add(locationRenderer.renderLocation(builder -> builder
//									.route(CodeFlowsPage.class) // FIXME
//									.routeParameters(new RouteParameters(WORKSPACE_REFERENCE_NAME, workspace.computeReference()))
//									.text("Configure")
//									.build()));
//						}
//						accordion.add(new AccordionPanel("Pipelines", pipelinesLayout));
					}
				}
				//
				if (workspace != null) {
					sidebar.add(locationRenderer.renderLocation(builder -> builder
							.route(FunctionsPage.class)
							.routeParameters(new RouteParameters(WORKSPACE_REFERENCE_NAME, workspace.computeReference()))
							.text("Functions")
							.build()));
					//
					sidebar.add(locationRenderer.renderLocation(builder -> builder
							.route(FunctionRunsPage.class)
							.routeParameters(new RouteParameters(WORKSPACE_REFERENCE_NAME, workspace.computeReference()))
							.text("Function Runs")
							.build()));
					//
					sidebar.add(locationRenderer.renderLocation(builder -> builder
							.route(RoutinesPage.class)
							.routeParameters(new RouteParameters(WORKSPACE_REFERENCE_NAME, workspace.computeReference()))
							.text("Routines")
							.build()));
				}
				//
				sidebar.add(locationRenderer.renderLocation(builder -> builder
						.route(workspace != null ? WorkspaceWebResourcesPage.class : RootWebResourcesPage.class)
						.routeParameters(workspace != null
								? new RouteParameters(WORKSPACE_REFERENCE_NAME, workspace.computeReference())
								: null)
						.text("Web Resources")
						.build()));
				//
				sidebar.add(locationRenderer.renderLocation(builder -> builder
						.route(RootUserAccountsPage.class)
						.route(workspace != null ? WorkspaceUserAccountsPage.class : RootUserAccountsPage.class)
						.routeParameters(workspace != null
								? new RouteParameters(WORKSPACE_REFERENCE_NAME, workspace.computeReference())
								: null)
						.text("User Accounts")
						.build()));
			}
		}
		return sidebar;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		stateObservable.set(State.builder()
				.currentLocation(event.getLocation())
				.workspaceReference(event.getRouteParameters()
						.get(WORKSPACE_REFERENCE_NAME)
						.orElse(null))
				.build());
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		add((Component) content, "content");
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class State {

		@NonNull
		Location currentLocation;

		@Nullable
		String workspaceReference;
	}
}
