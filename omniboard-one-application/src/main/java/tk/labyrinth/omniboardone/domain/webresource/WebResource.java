package tk.labyrinth.omniboardone.domain.webresource;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import lombok.experimental.FieldDefaults;
import org.bson.codecs.pojo.annotations.BsonId;
import tk.labyrinth.omniboardone.common.persistence.HasUid;
import tk.labyrinth.pandora.datatypes.simple.secret.Secret;

import java.util.UUID;

/**
 * <a href="https://en.wikipedia.org/wiki/Web_resource">https://en.wikipedia.org/wiki/Web_resource</a>
 */
@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@With
public class WebResource implements HasUid<WebResource> {

	// TODO: Maintain list of possible values.
	// TODO: Make suggests from url.
	// TODO: Probably rework into tags.
	String kind;

	String name;

	Secret token;

	@BsonId
	UUID uid;

	String url;
}
