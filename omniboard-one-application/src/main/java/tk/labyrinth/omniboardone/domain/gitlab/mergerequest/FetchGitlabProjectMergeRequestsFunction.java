package tk.labyrinth.omniboardone.domain.gitlab.mergerequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.data.binder.Binder;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.inject.Singleton;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProject;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSignature;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectWorker;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.functions.domain.function.Function;
import tk.labyrinth.pandora.functions.domain.function.FunctionParametersViewProvider;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.ClassBasedFunctionReference;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;
import tk.labyrinth.pandora.functions.domain.javafunction.JavaFunction;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.time.Instant;
import java.util.Objects;

@RequiredArgsConstructor
@Singleton
public class FetchGitlabProjectMergeRequestsFunction implements
		FunctionParametersViewProvider,
		JavaFunction<FetchGitlabProjectMergeRequestsFunction.Parameters, FetchGitlabProjectMergeRequestsFunction.Result> {

	public static final TypedFunctionReference<Parameters, Result> REFERENCE =
			new ClassBasedFunctionReference<>(FetchGitlabProjectMergeRequestsFunction.class);

	private final GitlabProjectWorker gitlabProjectWorker;

	private final ObjectMapper objectMapper;

	@Override
	public Result execute(ExecutionContext context, Parameters parameters) {
		val result = gitlabProjectWorker.fetchMergeRequests(parameters.projectSignature(), parameters.newOnly());
		//
		return Result.builder()
				.newMergeRequestIids(result._2())
				.updatedAfter(result._1())
				.updatedMergeRequestIids(result._3())
				.build();
	}

	@Override
	public TypedFunctionReference<Parameters, Result> getReference() {
		return REFERENCE;
	}

	@Override
	public ConfirmationHandle<Object> showFunctionParametersView(
			Map<String, Object> context,
			Function function,
			@Nullable Object parameters) {
		Workspace workspace = (Workspace) context.get("workspace").get();
		//
		Parameters castedParameters = objectMapper.convertValue(parameters, Parameters.class);
		//
		Binder<EditableParameters> binder = new Binder<>();
		binder.setBean(EditableParameters.builder()
				.newOnly(castedParameters != null ? castedParameters.newOnly() : true)
				.projectSignature(castedParameters != null ? castedParameters.projectSignature() : null)
				.build());
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
		}
		{
			{
				layout.add(new H3("Parameters"));
			}
			{
				Select<GitlabProjectSignature> projectSignatureSelect = new Select<>();
				//
				projectSignatureSelect.setItemLabelGenerator(item -> workspace.getGitlabProjectsOrEmpty()
						.filter(gitlabProject -> Objects.equals(gitlabProject.getSignature(), item))
						.head()
						.getUrl());
				projectSignatureSelect.setItems(workspace.getGitlabProjectsOrEmpty()
						.map(GitlabProject::getSignature)
						.filter(Objects::nonNull)
						.asJava());
				projectSignatureSelect.setLabel("Project Signature");
				//
				binder.bind(
						projectSignatureSelect,
						EditableParameters::projectSignature,
						EditableParameters::projectSignature);
				//
				layout.add(projectSignatureSelect);
			}
			{
				Checkbox newOnlyCheckbox = new Checkbox("New Only");
				//
				binder.bind(newOnlyCheckbox, EditableParameters::newOnly, EditableParameters::newOnly);
				//
				layout.add(newOnlyCheckbox);
			}
		}
		//
		return ConfirmationViews.showComponentDialog(
				layout,
				() -> {
					EditableParameters value = binder.getBean();
					//
					return Parameters.builder()
							.newOnly(value.newOnly())
							.projectSignature(value.projectSignature())
							.build();
				});
	}

	@Override
	public boolean supports(Function function) {
		return Objects.equals(function.getName(), getClass().getSimpleName());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Data
	@FieldDefaults(level = AccessLevel.PRIVATE)
	public static class EditableParameters {

		Boolean newOnly;

		GitlabProjectSignature projectSignature;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		Boolean newOnly;

		GitlabProjectSignature projectSignature;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Result {

		List<Long> newMergeRequestIids;

		@Nullable
		Instant updatedAfter;

		List<Long> updatedMergeRequestIids;
	}
}
