package tk.labyrinth.omniboardone.domain.gitlabproject;

import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.With;
import lombok.experimental.FieldDefaults;
import org.bson.codecs.pojo.annotations.BsonId;
import tk.labyrinth.omniboardone.domain.gitlab.GitlabDeploymentSnapshot;
import tk.labyrinth.omniboardone.domain.gitlab.GitlabEnvironmentSnapshot;
import tk.labyrinth.omniboardone.domain.gitlab.GitlabPipelineSnapshot;
import tk.labyrinth.omniboardone.domain.gitstate.GitBranchSnapshot;

import java.util.UUID;

@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@With
public class GitlabProjectSnapshot {

	@lombok.Builder.Default
	@NonNull
	List<GitBranchSnapshot> branches = List.empty();

	@lombok.Builder.Default
	@NonNull
	List<GitlabDeploymentSnapshot> deployments = List.empty();

	@lombok.Builder.Default
	@NonNull
	List<GitlabEnvironmentSnapshot> environments = List.empty();

	@lombok.Builder.Default
	@NonNull
	List<GitlabPipelineSnapshot> pipelines = List.empty();

	@NonNull
	GitlabProjectSignature signature;

	@BsonId
	UUID uid;

	String url;

	@NonNull
	UUID workspaceUid;
}
