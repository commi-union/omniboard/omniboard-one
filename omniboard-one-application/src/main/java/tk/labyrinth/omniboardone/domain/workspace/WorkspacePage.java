package tk.labyrinth.omniboardone.domain.workspace;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import io.vavr.control.Try;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.gitlabproject.EditableGitlabProject;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProject;
import tk.labyrinth.omniboardone.domain.gitlabproject.view.GitlabProjectGridView;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardRootLayout;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardWorkspaceLayout;
import tk.labyrinth.omniboardone.iam.AccessManager;
import tk.labyrinth.pandora.functionalvaadin.component.VaadinComponents;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.functionalvaadin.view.fault.FaultViews;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@RequiredArgsConstructor
@Route(value = "", layout = OmniboardWorkspaceLayout.class)
public class WorkspacePage extends Div implements BeforeEnterObserver {

	private final AccessManager accessManager;

	private final GitlabProjectGridView projectGridView;

	private final WorkspaceRepository workspaceRepository;

	@PostConstruct
	private void postConstruct() {
		setHeightFull();
	}

	private Component render(Workspace workspace) {
		boolean isAdmin = accessManager.isCurrentUserAdmin();
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
			//
			layout.setHeightFull();
		}
		{
			layout.addHorizontalLayout(toolbar -> {
				{
					toolbar.addClassName(PandoraStyles.LAYOUT);
				}
				//
				// FIXME: Rendering crutch
				toolbar.removeAll();
				//
				toolbar.add(new Span("Name: %s".formatted(workspace.getName())));
				//
				toolbar.addStretch();
				//
				toolbar.add(VaadinComponents.button(button -> {
					button.setEnabled(isAdmin);
					button.setText("Add Project");
					button.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
					//
					button.addClickListener(event -> {
						FormLayout formLayout = new FormLayout();
						Binder<EditableGitlabProject> binder = new Binder<>();
						binder.setBean(new EditableGitlabProject());
						{
							{
								formLayout.add(new H3("Add Project"));
							}
							{
								TextField urlField = new TextField("URL");
								//
								binder.bind(urlField, EditableGitlabProject::getUrl, EditableGitlabProject::setUrl);
								//
								formLayout.add(urlField);
							}
						}
						ConfirmationViews
								.showComponentDialog(formLayout)
								.subscribeAlwaysAccepted(success -> {
									EditableGitlabProject value = binder.getBean();
									//
									workspaceRepository.update(workspace.withGitlabProjects(
											workspace.getGitlabProjectsOrEmpty().append(GitlabProject.builder()
													.url(value.getUrl())
													.build())));
									//
									// TODO: We just need to refresh the content here.
									UI.getCurrent().getPage().reload();
								});
					});
				}));
			});
			//
			{
				projectGridView.render(GitlabProjectGridView.Parameters.builder()
						.projects(workspace.getGitlabProjectsOrEmpty())
						.workspace(workspace)
						.build());
				layout.add(projectGridView.asVaadinComponent());
			}
		}
		//
		UI.getCurrent().getPage().setTitle("Workspace %s | Omniboard".formatted(workspace.getName()));
		//
		return layout;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		Component component = Try.ofSupplier(() -> {
					Workspace workspace = workspaceRepository.findByReference(event
							.getRouteParameters()
							.get(OmniboardRootLayout.WORKSPACE_REFERENCE_NAME)
							.orElse(null));
					//
					return render(workspace);
				})
				.recover(FaultViews::createView)
				.get();
		//
		removeAll();
		//
		add(component);
	}
}
