package tk.labyrinth.omniboardone.domain.webresource;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardWorkspaceLayout;

@RequiredArgsConstructor
@Route(value = "webresources", layout = OmniboardWorkspaceLayout.class)
public class WorkspaceWebResourcesPage extends Composite<Component> {

	private final WebResourcesViewRenderer webResourcesViewRenderer;

	@Override
	protected Component initContent() {
		return webResourcesViewRenderer.render();
	}
}
