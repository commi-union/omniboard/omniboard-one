package tk.labyrinth.omniboardone.domain.webresource;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import io.vavr.collection.List;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.iam.AccessManager;
import tk.labyrinth.omniboardone.util.ViewUtils;
import tk.labyrinth.pandora.datatypes.simple.access.PandoraAccessLevel;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.sdk.ContainerWrapper;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.views.box.SecretBoxRenderer;
import tk.labyrinth.pandora.views.box.StringBoxRenderer;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@RequiredArgsConstructor
@Singleton
public class WebResourcesViewRenderer {

	private final AccessManager accessManager;

	private final WebResourceRepository webResourceRepository;

	private void showEditDialog(WebResource object) {
		boolean isAdmin = accessManager.isCurrentUserAdmin();
		//
		ConfirmationViews
				.showViewFunctionDialog(
						object,
						(next, sink) -> {
							ContainerWrapper<CssVerticalLayout> layout = FunctionalComponents.createContainer(CssVerticalLayout::new);
							{
								layout.addClassName(PandoraStyles.LAYOUT);
							}
							{
								{
									// FIXME
									layout.getContainer().getChildren()
											.filter(H3.class::isInstance)
											.forEach(Component::removeFromParent);
									layout.getElement().insertChild(
											0,
											new H3("%s Web Resource".formatted(object.getUid() != null ? "Edit" : "Create")).getElement());
								}
								{
									ContainerWrapper<CssVerticalLayout> formLayout = FunctionalComponents
											.createContainer(CssVerticalLayout::new);
									{
										formLayout.addClassName(PandoraStyles.LAYOUT);
										//
										formLayout.getContainer().setWidth("24em");
									}
									{
										formLayout.add(StringBoxRenderer.render(builder -> builder
												.accessLevel(isAdmin ? PandoraAccessLevel.EDITABLE : PandoraAccessLevel.VIEWABLE)
												.currentValue(next.getName())
												.initialValue(object.getName())
												.label("Name")
												.onValueChange(nextValue -> sink.accept(next.withName(nextValue)))
												.build()));
										//
										formLayout.add(StringBoxRenderer.render(builder -> builder
												.accessLevel(isAdmin ? PandoraAccessLevel.EDITABLE : PandoraAccessLevel.VIEWABLE)
												.currentValue(next.getUrl())
												.initialValue(object.getUrl())
												.label("URL")
												.onValueChange(nextValue -> sink.accept(next.withUrl(nextValue)))
												.build()));
										//
										formLayout.add(StringBoxRenderer.render(builder -> builder
												.accessLevel(isAdmin ? PandoraAccessLevel.EDITABLE : PandoraAccessLevel.VIEWABLE)
												.currentValue(next.getKind())
												.initialValue(object.getKind())
												.label("Kind")
												.onValueChange(nextValue -> sink.accept(next.withKind(nextValue)))
												.build()));
										//
										formLayout.add(SecretBoxRenderer.render(builder -> builder
												.accessLevel(isAdmin ? PandoraAccessLevel.EDITABLE : PandoraAccessLevel.LISTABLE)
												.currentValue(next.getToken())
												.initialValue(object.getToken())
												.label("Token")
												.onValueChange(nextValue -> sink.accept(next.withToken(nextValue)))
												.build()));
									}
									layout.add(formLayout.getContainer());
								}
								return layout.asView();
							}
						})
				.subscribeAlwaysAccepted(result ->
						ViewUtils.processFormConfirm(isAdmin, webResourceRepository, object, result));
	}

	public Component render() {
		boolean isAdmin = accessManager.isCurrentUserAdmin();
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			layout.addHorizontalLayout(toolbar -> {
				toolbar.addStretch();
				//
				toolbar.add(ButtonRenderer.render(builder -> builder
						.enabled(isAdmin)
						.onClick(event -> showEditDialog(WebResource.builder().build()))
						.text("Add")
						.themeVariants(ButtonVariant.LUMO_PRIMARY)
						.build()));
			});
			//
			{
				List<WebResource> webResources = webResourceRepository.findAllAsList();
				//
				Grid<WebResource> grid = ViewUtils.createGrid();
				{
					grid
							.addColumn(WebResource::getKind)
							.setHeader("Kind")
							.setResizable(true);
					grid
							.addColumn(WebResource::getName)
							.setHeader("Name")
							.setResizable(true);
					grid
							.addColumn(WebResource::getUrl)
							.setHeader("URL")
							.setResizable(true);
					grid
							.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
									.icon(VaadinIcon.EDIT.create())
									.onClick(event -> showEditDialog(item))
									.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
									.build()))
							.setFlexGrow(0)
							.setResizable(false);
				}
				{
					grid.setItems(webResources.asJava());
				}
				layout.add(grid);
			}
		}
		return layout;
	}
}
