package tk.labyrinth.omniboardone.domain.workspace;

import io.vavr.control.Try;
import jakarta.enterprise.context.ApplicationScoped;
import tk.labyrinth.omniboardone.tool.RepositoryBase;

import java.util.UUID;

@ApplicationScoped
public class WorkspaceRepository implements RepositoryBase<Workspace> {

	public Workspace findByReference(String reference) {
		Workspace result;
		{
			UUID uid = Try.ofSupplier(() -> UUID.fromString(reference)).getOrNull();
			//
			result = (uid != null
					? find("_id = ?1 or name = ?2", uid, reference)
					: find("name", reference))
					.firstResult();
		}
		return result;
	}

	public Workspace findByUid(UUID uid) {
		return find("_id", uid).firstResult();
	}

	public Workspace getByReference(String reference) {
		Workspace result = findByReference(reference);
		//
		if (result == null) {
			throw new IllegalArgumentException("Not found: reference = %s".formatted(reference));
		}
		//
		return result;
	}
}
