package tk.labyrinth.omniboardone.domain.delivery;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import tk.labyrinth.omniboardone.common.Hyperlink;

import java.time.Instant;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class DeliveryItem {

	Hyperlink commitLink;

	Hyperlink environmentLink;

	Instant happenedAt;

	Hyperlink refLink;

	String status;

	String text;
}
