package tk.labyrinth.omniboardone.domain.gitlabproject;

import io.vavr.collection.List;
import jakarta.enterprise.context.ApplicationScoped;
import tk.labyrinth.omniboardone.tool.RepositoryBase;

import java.util.UUID;

@ApplicationScoped
public class GitlabProjectSnapshotRepository implements RepositoryBase<GitlabProjectSnapshot> {

	public List<GitlabProjectSnapshot> findAllByWorkspaceUid(UUID workspaceUid) {
		return List.ofAll(find("workspaceUid", workspaceUid).list());
	}

	public GitlabProjectSnapshot findByUid(UUID uid) {
		return find("_id", uid).firstResult();
	}

	public GitlabProjectSnapshot findByWorkspaceUidAndProjectId(UUID workspaceUid, Long projectId) {
		return find("workspaceUid = ?1 and projectId = ?2", workspaceUid, projectId).firstResult();
	}
}
