package tk.labyrinth.omniboardone.domain.gitlab.mergerequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.data.binder.Binder;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.inject.Singleton;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSnapshot;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSnapshotRepository;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.functions.domain.function.Function;
import tk.labyrinth.pandora.functions.domain.function.FunctionParametersViewProvider;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.ClassBasedFunctionReference;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;
import tk.labyrinth.pandora.functions.domain.javafunction.JavaFunction;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.Objects;

@RequiredArgsConstructor
@Singleton
public class FetchAllGitlabProjectsMergeRequestsFunction implements
		FunctionParametersViewProvider,
		JavaFunction<FetchAllGitlabProjectsMergeRequestsFunction.Parameters, FetchAllGitlabProjectsMergeRequestsFunction.Result> {

	public static final TypedFunctionReference<Parameters, Result> REFERENCE =
			new ClassBasedFunctionReference<>(FetchAllGitlabProjectsMergeRequestsFunction.class);

	private final GitlabProjectSnapshotRepository gitlabProjectSnapshotRepository;

	private final ObjectMapper objectMapper;

	@Override
	public Result execute(ExecutionContext context, Parameters parameters) {
		List<GitlabProjectSnapshot> projectSnapshots = gitlabProjectSnapshotRepository.findAllAsList();
		//
		context.multiRunFunction(
				FetchGitlabProjectMergeRequestsFunction.REFERENCE,
				projectSnapshots.map(projectSnapshot -> FetchGitlabProjectMergeRequestsFunction.Parameters.builder()
						.newOnly(parameters.newOnly())
						.projectSignature(projectSnapshot.getSignature())
						.build()));
		//
		return Result.builder()
				.build();
	}

	@Override
	public TypedFunctionReference<Parameters, Result> getReference() {
		return REFERENCE;
	}

	@Override
	public ConfirmationHandle<Object> showFunctionParametersView(
			Map<String, Object> context,
			Function function,
			@Nullable Object parameters) {
		Parameters castedParameters = objectMapper.convertValue(parameters, Parameters.class);
		//
		Binder<EditableParameters> binder = new Binder<>();
		binder.setBean(EditableParameters.builder()
				.newOnly(castedParameters != null ? castedParameters.newOnly() : true)
				.build());
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
		}
		{
			{
				layout.add(new H3("Parameters"));
			}
			{
				Checkbox newOnlyCheckbox = new Checkbox("New Only");
				//
				binder.bind(newOnlyCheckbox, EditableParameters::newOnly, EditableParameters::newOnly);
				//
				layout.add(newOnlyCheckbox);
			}
		}
		//
		return ConfirmationViews.showComponentDialog(
				layout,
				() -> {
					EditableParameters value = binder.getBean();
					//
					return Parameters.builder()
							.newOnly(value.newOnly())
							.build();
				});
	}

	@Override
	public boolean supports(Function function) {
		return Objects.equals(function.getName(), getClass().getSimpleName());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Data
	@FieldDefaults(level = AccessLevel.PRIVATE)
	public static class EditableParameters {

		Boolean newOnly;
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		Boolean newOnly;
	}

	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Result {
		// empty
	}
}
