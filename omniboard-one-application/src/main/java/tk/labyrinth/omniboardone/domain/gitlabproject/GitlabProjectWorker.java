package tk.labyrinth.omniboardone.domain.gitlabproject;

import io.vavr.Tuple;
import io.vavr.Tuple3;
import io.vavr.collection.List;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.gitlab4j.api.models.ApprovalState;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.Environment;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.Pipeline;
import org.gitlab4j.api.models.Project;
import tk.labyrinth.omniboardone.domain.gitlab.GitlabConnector;
import tk.labyrinth.omniboardone.domain.gitlab.GitlabDeploymentSnapshot;
import tk.labyrinth.omniboardone.domain.gitlab.GitlabEnvironmentSnapshot;
import tk.labyrinth.omniboardone.domain.gitlab.GitlabPipelineSnapshot;
import tk.labyrinth.omniboardone.domain.gitlab.mergerequest.GitlabMergeRequestSnapshot;
import tk.labyrinth.omniboardone.domain.gitlab.mergerequest.GitlabMergeRequestSnapshotRepository;
import tk.labyrinth.omniboardone.domain.gitstate.GitBranchSnapshot;
import tk.labyrinth.omniboardone.domain.gitstate.GitRevision;
import tk.labyrinth.omniboardone.domain.webresource.WebResource;
import tk.labyrinth.omniboardone.domain.webresource.WebResourceRepository;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.domain.workspace.WorkspaceRepository;
import tk.labyrinth.pandora.datatypes.simple.secret.Secret;

import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
@RequiredArgsConstructor
@Slf4j
public class GitlabProjectWorker {

	private final GitlabMergeRequestSnapshotRepository gitlabMergeRequestSnapshotRepository;

	private final GitlabProjectSnapshotRepository gitlabProjectSnapshotRepository;

	private final WebResourceRepository webResourceRepository;

	private final WorkspaceRepository workspaceRepository;

	private GitlabConnector createConnector(String projectUrl) {
		String baseUrl = GitlabProjectUtils.extractBaseUrl(projectUrl);
		//
		WebResource webResource = webResourceRepository.findByUrl(baseUrl);
		//
		return GitlabConnector.builder()
				.accessToken(Optional.ofNullable(webResource).map(WebResource::getToken).map(Secret::getValue).orElse(null))
				.baseUrl(baseUrl)
				.build();
	}

	public void discoverProject(Workspace workspace, String projectUrl) {
		String baseUrl = GitlabProjectUtils.extractBaseUrl(projectUrl);
		//
		GitlabConnector gitlabConnector = createConnector(baseUrl);
		//
		Project project = gitlabConnector.findProject(URI.create(projectUrl).getPath().substring(1));
		//
		GitlabProject nextProject = GitlabProject.builder()
				.signature(GitlabProjectSignature.builder()
						.baseUrl(baseUrl)
						.id(project.getId())
						.build())
				.url(project.getWebUrl())
				.build();
		//
		workspaceRepository.update(workspace.withGitlabProjects(workspace.getGitlabProjects()
				.map(gitlabProject -> Objects.equals(gitlabProject.getUrl(), project.getWebUrl())
						? nextProject
						: gitlabProject)));
		//
		refreshProjectSnapshot(workspace.getUid(), nextProject);
	}

	public void fetchMergeRequest(GitlabProjectSignature projectSignature, Long mergeRequestIid) {
		GitlabConnector gitlabConnector = createConnector(projectSignature.getBaseUrl());
		//
		Pair<MergeRequest, ApprovalState> mergeRequestAndApprovalState = gitlabConnector
				.getMergeRequestAndApprovalState(projectSignature.getId(), mergeRequestIid);
		MergeRequest mergeRequest = mergeRequestAndApprovalState.getLeft();
		ApprovalState approvalState = mergeRequestAndApprovalState.getRight();
		//
		GitlabMergeRequestSnapshot foundSnapshot = gitlabMergeRequestSnapshotRepository.findByGitlabIid(
				mergeRequest.getIid());
		//
		gitlabMergeRequestSnapshotRepository.persistOrUpdate(GitlabMergeRequestSnapshot.builder()
				.approvalState(approvalState)
				.mergeRequest(mergeRequest)
				.mergeRequestSignature("%s:%s".formatted(mergeRequest.getIid(), mergeRequest.getUpdatedAt().toInstant()))
				.projectSignature(projectSignature)
				.uid(foundSnapshot != null ? foundSnapshot.getUid() : UUID.randomUUID())
				.build());
	}

	public Tuple3<@Nullable Instant, List<Long>, List<Long>> fetchMergeRequests(
			GitlabProjectSignature projectSignature,
			boolean newOnly) {
		logger.info("fetchMergeRequests: %s".formatted(projectSignature));
		//
		Instant updatedAfter = newOnly
				? gitlabMergeRequestSnapshotRepository.findLastUpdatedAt(projectSignature)
				: null;
		//
		logger.info("updatedAfter: %s".formatted(updatedAfter));
		//
		GitlabConnector gitlabConnector = createConnector(projectSignature.getBaseUrl());
		//
		List<MergeRequest> mergeRequests = gitlabConnector.getMergeRequests(projectSignature.getId(), updatedAfter);
		//
		logger.info("mergeRequests.size: %s".formatted(mergeRequests.size()));
		//
		ArrayList<Long> foundIids = new ArrayList<>();
		mergeRequests.forEach(mergeRequest -> {
			GitlabMergeRequestSnapshot foundSnapshot = gitlabMergeRequestSnapshotRepository.findByGitlabIid(
					mergeRequest.getIid());
			//
			if (foundSnapshot != null) {
				foundIids.add(foundSnapshot.getMergeRequest().getIid());
			}
			//
			logger.info("mergeRequest: iid = %s, found = %s".formatted(mergeRequest.getIid(), foundSnapshot != null));
			//
			gitlabMergeRequestSnapshotRepository.persistOrUpdate(GitlabMergeRequestSnapshot.builder()
					.mergeRequest(mergeRequest)
					.mergeRequestSignature("%s:%s".formatted(mergeRequest.getIid(), mergeRequest.getUpdatedAt().toInstant()))
					.projectSignature(projectSignature)
					.uid(foundSnapshot != null ? foundSnapshot.getUid() : UUID.randomUUID())
					.build());
		});
		//
		val updatedAndCreatedMergeRequestIids = mergeRequests.map(MergeRequest::getIid).partition(foundIids::contains);
		//
		logger.info("done");
		//
		return Tuple.of(
				updatedAfter,
				updatedAndCreatedMergeRequestIids._2(),
				updatedAndCreatedMergeRequestIids._1());
	}

	public void refreshProjectSnapshot(UUID workspaceUid, GitlabProject project) {
		GitlabProjectSignature projectSignature = project.getSignature();
		//
		GitlabConnector gitlabConnector = createConnector(projectSignature.getBaseUrl());
		//
		List<Branch> branches = gitlabConnector.getBranches(projectSignature.getId());
		List<Environment> environments = gitlabConnector.getEnvironments(projectSignature.getId());
		List<Pipeline> pipelines = gitlabConnector.getPipelines(projectSignature.getId());
		//
		GitlabProjectSnapshot gitlabProjectSnapshot = gitlabProjectSnapshotRepository
				.findByWorkspaceUidAndProjectId(workspaceUid, projectSignature.getId());
		//
		gitlabProjectSnapshotRepository.persistOrUpdate(
				GitlabProjectSnapshot.builder()
						.branches(branches.map(branch -> GitBranchSnapshot.builder()
								.head(GitRevision.builder()
										.createdAt(branch.getCommit().getCreatedAt().toInstant())
										.hash(branch.getCommit().getId())
										.build())
								.name(branch.getName())
								.build()))
						.deployments(environments
								.map(Environment::getLastDeployment)
								.filter(Objects::nonNull)
								.map(deployment -> GitlabDeploymentSnapshot.builder()
										.createdAt(deployment.getCreatedAt().toInstant())
										.gitlabDeployment(deployment)
										.gitlabId(deployment.getId())
										.ref(deployment.getRef())
										.sha(deployment.getSha())
										.status(deployment.getStatus())
										.updatedAt(deployment.getUpdatedAt().toInstant())
										.build()))
						.environments(environments.map(environment -> GitlabEnvironmentSnapshot.builder()
								.gitlabId(environment.getId())
								.lastDeploymentGitlabId(environment.getLastDeployment() != null
										? environment.getLastDeployment().getId()
										: null)
								.name(environment.getName())
								.slug(environment.getSlug())
								.state(environment.getState())
								.build()))
						.pipelines(pipelines.map(pipeline -> GitlabPipelineSnapshot.builder()
								.createdAt(pipeline.getCreatedAt().toInstant())
								.gitlabId(pipeline.getId())
								.gitlabPipeline(pipeline)
								.ref(pipeline.getRef())
								.sha(pipeline.getSha())
								.source(pipeline.getSource())
								.status(pipeline.getStatus())
								.updatedAt(pipeline.getUpdatedAt().toInstant())
								.build()))
						.signature(project.getSignature())
						.uid(gitlabProjectSnapshot != null ? gitlabProjectSnapshot.getUid() : UUID.randomUUID())
						.url(project.getUrl())
						.workspaceUid(workspaceUid)
						.build());
	}
}
