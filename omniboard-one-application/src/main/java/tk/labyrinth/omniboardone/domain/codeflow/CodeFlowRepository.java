package tk.labyrinth.omniboardone.domain.codeflow;

import io.vavr.collection.List;
import io.vavr.control.Try;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Singleton;
import tk.labyrinth.omniboardone.tool.RepositoryBase;

import java.util.UUID;

@Singleton
public class CodeFlowRepository implements RepositoryBase<CodeFlow> {

	public List<CodeFlow> findAllByWorkspaceUid(UUID workspaceUid) {
		return List.ofAll(find("workspaceUid", workspaceUid).list());
	}

	public CodeFlow findByReference(String reference) {
		CodeFlow result;
		{
			UUID uid = Try.ofSupplier(() -> UUID.fromString(reference)).getOrNull();
			//
			result = (uid != null
					? find("_id = ?1 or slug = ?2", uid, reference)
					: find("slug", reference))
					.firstResult();
		}
		return result;
	}

	public CodeFlow findByUid(UUID uid) {
		return find("_id", uid).firstResult();
	}
}
