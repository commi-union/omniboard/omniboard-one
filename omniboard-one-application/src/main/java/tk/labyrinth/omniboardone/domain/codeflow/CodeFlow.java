package tk.labyrinth.omniboardone.domain.codeflow;

import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import lombok.experimental.FieldDefaults;
import org.bson.codecs.pojo.annotations.BsonId;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProjectSignature;

import java.util.UUID;

@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@With
public class CodeFlow {

	String name;

	List<GitlabProjectSignature> projectSignatures;

	String slug;

	List<CodeFlowStage> stages;

	@BsonId
	UUID uid;

	UUID workspaceUid;
}
