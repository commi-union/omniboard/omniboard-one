package tk.labyrinth.omniboardone.domain.gitlabproject;

import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Singleton
@Slf4j
public class GitlabProjectSnapshotCleaner {

	private final GitlabProjectSnapshotRepository gitlabProjectSnapshotRepository;

	private void onStartup(@Observes StartupEvent ignored) {
		long deletedCount = gitlabProjectSnapshotRepository.delete("signature is null");
		//
		logger.info("Deleted: {}", deletedCount);
	}
}
