package tk.labyrinth.omniboardone.domain.sidebar;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.RouterLink;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.functionalvaadin.component.RouterLinkRenderer;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.BackgroundColor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.Objects;
import java.util.function.Function;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SmartLocationRenderer {

	private final String currentLocation;

	public Component renderLocation(
			Function<RouterLinkRenderer.Parameters.Builder, RouterLinkRenderer.Parameters> parametersConfigurer) {
		CssHorizontalLayout layout = new CssHorizontalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT_SMALL);
			//
			layout.getStyle().set("padding-top", "0.25em");
			layout.getStyle().set("padding-bottom", "0.25em");
			//
			layout.setAlignItems(AlignItems.CENTER);
		}
		{
//			Div bullet = new Div();
			//
			{
				RouterLink routerLink = RouterLinkRenderer.render(parametersConfigurer);
				//
				CssFlexItem.setFlexGrow(routerLink, 1);
				//
				if (Objects.equals(routerLink.getHref(), currentLocation)) {
					StyleUtils.setCssProperty(layout, BackgroundColor.of("var(--lumo-contrast-10pct)"));
//					StyleUtils.setCssProperty(bullet, BackgroundColor.of("var(--lumo-primary-color)"));
				}
				//
				layout.add(routerLink);
			}
//			{
//				bullet.getStyle().set("border-radius", "0.5em");
//				bullet.getStyle().set("height", "0.5em");
//				bullet.getStyle().set("width", "0.5em");
//				//
//				layout.add(bullet);
//			}
		}
		return layout;
	}

	public static SmartLocationRenderer of(String currentLocation) {
		return new SmartLocationRenderer(currentLocation);
	}
}
