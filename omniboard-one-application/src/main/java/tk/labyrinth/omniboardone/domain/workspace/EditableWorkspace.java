package tk.labyrinth.omniboardone.domain.workspace;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import tk.labyrinth.omniboardone.domain.gitlabproject.GitlabProject;

import java.util.List;
import java.util.UUID;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EditableWorkspace {

	List<GitlabProject> gitlabProjects;

	String name;

	UUID uid;
}
