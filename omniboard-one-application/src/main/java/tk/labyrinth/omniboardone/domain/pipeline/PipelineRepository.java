package tk.labyrinth.omniboardone.domain.pipeline;

import io.vavr.collection.List;
import io.vavr.control.Try;
import jakarta.enterprise.context.ApplicationScoped;
import tk.labyrinth.omniboardone.tool.RepositoryBase;

import java.util.UUID;

@ApplicationScoped
public class PipelineRepository implements RepositoryBase<Pipeline> {

	public List<Pipeline> findAllByWorkspaceUid(UUID workspaceUid) {
		return List.ofAll(find("workspaceUid", workspaceUid).list());
	}

	public Pipeline findByReference(String reference) {
		Pipeline result;
		{
			UUID uid = Try.ofSupplier(() -> UUID.fromString(reference)).getOrNull();
			//
			result = (uid != null
					? find("_id = ?1 or slug = ?2", uid, reference)
					: find("slug", reference))
					.firstResult();
		}
		return result;
	}

	public Pipeline findByUid(UUID uid) {
		return find("_id", uid).firstResult();
	}
}
