package tk.labyrinth.omniboardone.domain.webresource;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardRootLayout;

@RequiredArgsConstructor
//@RouteAlias("")
@Route(value = "webresources", layout = OmniboardRootLayout.class)
public class RootWebResourcesPage extends Composite<Component> {

	private final WebResourcesViewRenderer webResourcesViewRenderer;

	@Override
	protected Component initContent() {
		return webResourcesViewRenderer.render();
	}
}
