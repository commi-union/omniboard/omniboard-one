package tk.labyrinth.omniboardone.tool;

import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.vavr.collection.List;

public interface RepositoryBase<Entity> extends PanacheMongoRepository<Entity> {

	default List<Entity> findAllAsList() {
		return List.ofAll(findAll().list());
	}

	default List<Entity> findAllByEnabled(Boolean enabled) {
		return List.ofAll(find("enabled", enabled).list());
	}
}
