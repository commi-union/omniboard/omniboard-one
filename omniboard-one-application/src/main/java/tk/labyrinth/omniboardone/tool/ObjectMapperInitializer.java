package tk.labyrinth.omniboardone.tool;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Singleton;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperFactory;
import tk.labyrinth.pandora.misc4j.lib.jackson.VavrObjectMapperConfigurer;

public class ObjectMapperInitializer {

	@Singleton
	public static ObjectMapper objectMapper(Instance<ObjectMapperConfigurer> objectMapperConfigurers) {
		return ObjectMapperFactory.defaultConfigured(objectMapperConfigurers.stream().toList())
				// FIXME: Think if we want this as default feature.
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	@Singleton
	public static VavrObjectMapperConfigurer vavrObjectMapperConfigurer() {
		return new VavrObjectMapperConfigurer();
	}
}
