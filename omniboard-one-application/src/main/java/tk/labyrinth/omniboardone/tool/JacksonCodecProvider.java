package tk.labyrinth.omniboardone.tool;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.io.IOContext;
import com.fasterxml.jackson.core.util.BufferRecycler;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoException;
import io.vavr.collection.List;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.bson.AbstractBsonReader;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.UuidRepresentation;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.pojo.PropertyCodecProvider;
import org.bson.codecs.pojo.PropertyCodecRegistry;
import org.bson.codecs.pojo.TypeWithTypeParameters;
import org.mongojack.MongoJsonMappingException;
import org.mongojack.internal.stream.DBDecoderBsonParser;
import org.mongojack.internal.stream.DBEncoderBsonGenerator;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.UUID;

@ApplicationScoped
@RequiredArgsConstructor
public class JacksonCodecProvider implements PropertyCodecProvider {

	private static final InputStream emptyInputStream = new EmptyInputStream();

	private final ObjectMapper objectMapper;

	@Override
	@SuppressWarnings("unchecked")
	public <T> Codec<T> get(TypeWithTypeParameters<T> type, PropertyCodecRegistry registry) {
		Codec<T> result;
		{
			if (type.getType() != UUID.class) {
				result = (Codec<T>) new JacksonCodec(toType(type));
			} else {
				result = null;
			}
		}
		return result;
	}

	public static Type toType(TypeWithTypeParameters<?> type) {
		return !type.getTypeParameters().isEmpty()
				? TypeUtils.parameterize(
				type.getType(),
				List.ofAll(type.getTypeParameters()).map(JacksonCodecProvider::toType).toJavaArray(Type[]::new))
				: type.getType();
	}

	@RequiredArgsConstructor
	public class JacksonCodec implements Codec<Object> {

		private final Type type;

		@Override
		public Object decode(BsonReader reader, DecoderContext decoderContext) {
			//noinspection deprecation
			try (DBDecoderBsonParser parser = new DBDecoderBsonParser(
					new IOContext(new BufferRecycler(), emptyInputStream, false),
					0,
					(AbstractBsonReader) reader,
					objectMapper,
					UuidRepresentation.STANDARD)) {
				return objectMapper.reader().forType(objectMapper.getTypeFactory().constructType(type)).readValue(parser);
			} catch (IOException ex) {
				throw new RuntimeException("IOException encountered while parsing", ex);
			}
		}

		@Override
		public void encode(BsonWriter writer, Object value, EncoderContext encoderContext) {
			try (JsonGenerator generator = new DBEncoderBsonGenerator(writer, UuidRepresentation.STANDARD)) {
				objectMapper.writeValue(generator, value);
			} catch (JsonMappingException ex) {
				throw new MongoJsonMappingException(ex);
			} catch (IOException ex) {
				throw new MongoException("Error writing object out", ex);
			}
		}

		@Override
		@SuppressWarnings("unchecked")
		public Class<Object> getEncoderClass() {
			return (Class<Object>) tk.labyrinth.pandora.misc4j.java.lang.reflect.TypeUtils.getClass(type);
		}
	}

	private static class EmptyInputStream extends InputStream {

		@Override
		public int available() {
			return 0;
		}

		@Override
		public int read() {
			return -1;
		}
	}
}
