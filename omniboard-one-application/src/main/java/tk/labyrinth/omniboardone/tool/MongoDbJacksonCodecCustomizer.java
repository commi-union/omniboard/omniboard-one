package tk.labyrinth.omniboardone.tool;

import com.mongodb.MongoClientSettings;
import io.quarkus.mongodb.runtime.MongoClientCustomizer;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import org.bson.UuidRepresentation;

@ApplicationScoped
@RequiredArgsConstructor
public class MongoDbJacksonCodecCustomizer implements MongoClientCustomizer {

	@Override
	public MongoClientSettings.Builder customize(MongoClientSettings.Builder builder) {
		return builder.uuidRepresentation(UuidRepresentation.STANDARD);
	}
}
