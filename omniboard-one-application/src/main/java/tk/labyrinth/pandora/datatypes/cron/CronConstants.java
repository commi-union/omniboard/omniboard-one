package tk.labyrinth.pandora.datatypes.cron;

import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinition;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;

public class CronConstants {

	public static final CronDefinition CRON_DEFINITION = CronDefinitionBuilder.instanceDefinitionFor(CronType.SPRING53);

	public static final CronDescriptor CRON_DESCRIPTOR = CronDescriptor.instance();

	public static final CronParser CRON_PARSER = new CronParser(CRON_DEFINITION);
}
