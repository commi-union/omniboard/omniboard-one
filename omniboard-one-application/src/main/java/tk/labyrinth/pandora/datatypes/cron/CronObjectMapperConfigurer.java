package tk.labyrinth.pandora.datatypes.cron;

import com.cronutils.model.Cron;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import jakarta.inject.Singleton;
import tk.labyrinth.pandora.misc4j.lib.jackson.ObjectMapperConfigurer;

import java.io.IOException;

@Singleton
public class CronObjectMapperConfigurer implements ObjectMapperConfigurer {

	@Override
	public ObjectMapper configure(ObjectMapper objectMapper) {
		{
			SimpleModule module = new SimpleModule(getClass().getName());
			{
				registerWith(module, objectMapper);
			}
			objectMapper.registerModule(module);
		}
		return objectMapper;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public static SimpleModule registerWith(SimpleModule module, ObjectMapper objectMapper) {
		JsonDeserializer deserializer = new Deserializer();
		//
		module.addDeserializer(deserializer.handledType(), deserializer);
		module.addSerializer(new Serializer());
		//
		return module;
	}

	public static class Deserializer extends StdDeserializer<Cron> {

		public Deserializer() {
			super(Cron.class);
		}

		@Override
		public Cron deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
			return CronConstants.CRON_PARSER.parse(p.getText());
		}
	}

	public static class Serializer extends StdSerializer<Cron> {

		public Serializer() {
			super(Cron.class);
		}

		@Override
		public void serialize(Cron value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeString(value.asString());
		}
	}
}
