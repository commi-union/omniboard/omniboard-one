package tk.labyrinth.pandora.extra;

import jakarta.enterprise.inject.Produces;
import jakarta.inject.Singleton;
import tk.labyrinth.pandora.datatypes.fault.FaultObjectMapperConfigurer;
import tk.labyrinth.pandora.datatypes.outcome.OutcomeObjectMapperConfigurer;
import tk.labyrinth.pandora.datatypes.simple.secret.SecretObjectMapperConfigurer;
import tk.labyrinth.pandora.dependencies.jaap.PackageSignatureObjectMapperConfigurer;

@Deprecated // These should be provided by Pandora implicitly.
public class PandoraBeansProvider {

	@Produces
	@Singleton
	public static FaultObjectMapperConfigurer faultObjectMapperConfigurer() {
		return new FaultObjectMapperConfigurer();
	}

	@Produces
	@Singleton
	public static OutcomeObjectMapperConfigurer outcomeObjectMapperConfigurer() {
		return new OutcomeObjectMapperConfigurer();
	}

	@Produces
	@Singleton
	public static PackageSignatureObjectMapperConfigurer packageSignatureObjectMapperConfigurer() {
		return new PackageSignatureObjectMapperConfigurer();
	}

	@Produces
	@Singleton
	public static SecretObjectMapperConfigurer secretObjectMapperConfigurer() {
		return new SecretObjectMapperConfigurer();
	}
}
