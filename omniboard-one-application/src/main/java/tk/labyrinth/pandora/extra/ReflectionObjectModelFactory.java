package tk.labyrinth.pandora.extra;

import io.vavr.collection.List;
import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import org.jibx.schema.codegen.extend.DefaultNameConverter;
import org.jibx.schema.codegen.extend.NameConverter;
import tk.labyrinth.jaap.model.declaration.TypeDescription;
import tk.labyrinth.pandora.datatypes.javabasetype.JavaBaseTypeUtils;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModelUtils;
import tk.labyrinth.pandora.datatypes.objectmodel.attribute.ObjectModelAttribute;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

@LazyComponent
@RequiredArgsConstructor
public class ReflectionObjectModelFactory {

	private static final NameConverter nameConverter = new DefaultNameConverter();

	public ObjectModel createModelFromJavaClass(Class<?> javaClass) {
		String modelCode = resolveModelCode(javaClass);
		//
		return ObjectModel.builder()
				.attributes(resolveAttributes(javaClass))
				.code(modelCode)
				.features(null)
				.name(resolveName(javaClass))
				.parameterNames(javaClass.getTypeParameters().length > 0
						? List.of(javaClass.getTypeParameters()).map(TypeVariable::getName)
						: null)
				.tags(null)
				.build();
	}

	public String getAttributeName(Method method) {
		String result = ObjectModelUtils.findAttributeName(method);
		if (result == null) {
			throw new IllegalArgumentException("Not found: method = %s".formatted(method));
		}
		return result;
	}

	public TypeDescription getTypeDescriptionOf(Member attributeMember) {
		return TypeDescription.of(getTypeOf(attributeMember));
	}

	public Type getTypeOf(Member member) {
		Type result;
		{
			if (member instanceof Field field) {
				result = field.getGenericType();
			} else if (member instanceof Method method) {
				result = method.getGenericReturnType();
			} else {
				throw new NotImplementedException();
			}
		}
		return result;
	}

	public List<ObjectModelAttribute> resolveAttributes(Class<?> javaClass) {
		List<Member> attributeMembers = selectAttributeMembers(javaClass);
		//
		return attributeMembers
				.map(attributeMember -> ObjectModelAttribute.builder()
						.datatype(JavaBaseTypeUtils.createDatatypeFromTypeDescription(getTypeDescriptionOf(attributeMember)))
						.features(null)
						.name(ObjectModelUtils.getAttributeName(attributeMember))
						.build())
				.sortBy(ObjectModelAttribute::getName);
	}

	public String resolveName(Class<?> javaClass) {
		return javaClass.getSimpleName();
	}

	public List<Member> selectAttributeMembers(Class<?> javaClass) {
		List<Member> result;
		{
			if (javaClass.isInterface()) {
				// Selecting methods where 'getX' -> 'x'.
				//
				result = Stream.of(javaClass.getDeclaredMethods())
						.filter(declaredMethod -> !Modifier.isStatic(declaredMethod.getModifiers()))
						.filter(declaredMethod -> !declaredMethod.isDefault())
						.filter(declaredMethod -> ObjectModelUtils.findAttributeName(declaredMethod) != null)
						.map(Member.class::cast)
						.toList();
			} else {
				// Selecting fields.
				//
				result = Stream.of(javaClass.getDeclaredFields())
						.filter(declaredField -> !Modifier.isStatic(declaredField.getModifiers()))
						.map(Member.class::cast)
						.toList();
			}
		}
		return result;
	}

	public static String resolveModelCode(Class<?> javaClass) {
		return javaClass.getSimpleName().toLowerCase();
	}
}
