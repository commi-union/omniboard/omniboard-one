package tk.labyrinth.pandora.views;

import com.cronutils.model.Cron;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextField;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.simple.access.PandoraAccessLevel;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.TextFieldRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;

import java.util.function.Consumer;
import java.util.function.Function;

public class CronBoxRenderer {

	public static View render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public static View render(Parameters parameters) {
		TextField textField = TextFieldRenderer.render(builder -> builder
				.label(parameters.label())
				.readOnly(true)
				.suffixComponent(ButtonRenderer.render(buttonBuilder -> buttonBuilder
						.icon(VaadinIcon.COG.create())
						.onClick(event -> {
							Cron initialValue = parameters.initialValue() != null
									? parameters.initialValue()
									: CronViewRenderer.getDefaultValue();
							//
							ConfirmationViews
									.showViewFunctionDialog(
											initialValue,
											(next, sink) -> CronViewRenderer.render(innerBuilder -> innerBuilder
													.accessLevel(parameters.accessLevel())
													.currentValue(next)
													.initialValue(initialValue)
													.onValueChange(sink)
													.build()))
									.subscribeAlwaysAccepted(parameters.onValueChange());
						})
						.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
						.build()))
				.value(parameters.currentValue() != null ? parameters.currentValue().asString() : "")
				.build());
		//
		return ComponentView.of(textField);
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		PandoraAccessLevel accessLevel;

		@Nullable
		Cron currentValue;

		@Nullable
		Cron initialValue;

		String label;

		@NonNull
		Consumer<@Nullable Cron> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}
