package tk.labyrinth.pandora.views;

import com.cronutils.builder.CronBuilder;
import com.cronutils.model.Cron;
import com.cronutils.model.field.CronField;
import com.cronutils.model.field.CronFieldName;
import com.cronutils.model.field.definition.FieldDefinition;
import com.cronutils.model.time.ExecutionTime;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import io.vavr.Tuple;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.cron.CronConstants;
import tk.labyrinth.pandora.datatypes.simple.access.PandoraAccessLevel;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.component.TextFieldRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.View;
import tk.labyrinth.pandora.misc4j.java.lang.ObjectUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssgrid.CssGridLayout;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * <a href="https://github.com/jmrozanec/cron-utils">https://github.com/jmrozanec/cron-utils</a>
 */
public class CronViewRenderer {

	private final static DateTimeFormatter cronFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd E HH:mm:ss");

	public static Cron computeCron(Cron currentCron, CronFieldName cronFieldName, String value) {
		String cronString = List.of(CronFieldName.values())
				.filter(CronConstants.CRON_DEFINITION::containsFieldDefinition)
				.map(cronFieldNameElement -> cronFieldNameElement == cronFieldName
						? value
						: getCronFieldValue(currentCron, cronFieldNameElement))
				.mkString(" ");
		//
		return CronConstants.CRON_PARSER.parse(cronString);
	}

	public static String getCronFieldValue(Cron cron, CronFieldName cronFieldName) {
		CronField cronField = cron.retrieveFieldsAsMap().get(cronFieldName);
		//
		return cronField != null ? cronField.getExpression().asString() : getDefaultValue(cronFieldName);
	}

	public static Cron getDefaultValue() {
		return CronBuilder.daily(CronConstants.CRON_DEFINITION);
	}

	public static String getDefaultValue(CronFieldName cronFieldName) {
		String result;
		{
			FieldDefinition fieldDefinition = CronConstants.CRON_DEFINITION.getFieldDefinition(cronFieldName);
			//
			if (fieldDefinition != null) {
				result = !fieldDefinition.isOptional() ? "*" : "";
			} else {
				result = "";
			}
		}
		return result;
	}

	public static <T> View render(Parameters parameters) {
		CssGridLayout layout = FunctionalComponents.createComponent(CssGridLayout::new);
		{
			layout.addClassName(PandoraStyles.LAYOUT);
			//
			layout.setGridTemplateAreas(
					// TODO: defaults (macros), timezone
					"full			.				.				before",
					"second		minute	hour		before",
					"dow			dom			doy			after",
					"month		year		.				after",
					"pretty		pretty	pretty	pretty");
		}
		{
			layout.add(TextFieldRenderer.render(builder -> builder
					.label("Full")
					.readOnly(true)
					.value(parameters.currentValue().asString())
					.build()));
			//
			List
					.of(
							Tuple.of(CronFieldName.SECOND, "Second", "second"),
							Tuple.of(CronFieldName.MINUTE, "Minute", "minute"),
							Tuple.of(CronFieldName.HOUR, "Hour", "hour"),
							Tuple.of(CronFieldName.DAY_OF_WEEK, "Day of Week", "dow"),
							Tuple.of(CronFieldName.DAY_OF_MONTH, "Day of Month", "dom"),
							Tuple.of(CronFieldName.DAY_OF_YEAR, "Day of Year", "doy"),
							Tuple.of(CronFieldName.MONTH, "Month", "month"),
							Tuple.of(CronFieldName.YEAR, "Year", "year"))
					.forEach(tuple -> layout.add(
							TextFieldRenderer.render(builder -> builder
									.enabled(CronConstants.CRON_DEFINITION.containsFieldDefinition(tuple._1()))
									.label(tuple._2())
									.onValueChange(nextValue -> parameters.onValueChange().accept(computeCron(
											parameters.currentValue(),
											tuple._1(),
											nextValue)))
									.readOnly(!PandoraAccessLevel.canEdit(parameters.accessLevel()))
									.value(getCronFieldValue(parameters.currentValue(), tuple._1()))
									.build()),
							tuple._3()));
			//
			{
				// FIXME: Crutch to make functional rendering work.
				layout.getChildren()
						.filter(Span.class::isInstance)
						.forEach(Component::removeFromParent);
				//
				layout.add(
						new Span(CronConstants.CRON_DESCRIPTOR.describe(parameters.currentValue())),
						"pretty");
			}
			{
				ExecutionTime executionTime = ExecutionTime.forCron(parameters.currentValue());
				ZonedDateTime now = Instant.now().atOffset(ZoneOffset.UTC).toZonedDateTime();
				//
				// FIXME: Crutch to make functional rendering work.
				layout.getChildren()
						.filter(CssVerticalLayout.class::isInstance)
						.forEach(Component::removeFromParent);
				//
				layout.add(
						ObjectUtils.consumeAndReturn(
								new CssVerticalLayout(),
								beforeLayout -> {
									ZonedDateTime localNow = now;
									for (int i = 0; i < 5; i++) {
										ZonedDateTime before = executionTime.lastExecution(localNow).orElseThrow();
										//
										beforeLayout.getElement().insertChild(
												0,
												new Span(cronFormatter.format(before)).getElement());
										//
										localNow = before;
									}
								}),
						"before");
				layout.add(
						ObjectUtils.consumeAndReturn(
								new CssVerticalLayout(),
								afterLayout -> {
									ZonedDateTime localNow = now;
									for (int i = 0; i < 5; i++) {
										ZonedDateTime after = executionTime.nextExecution(localNow).orElseThrow();
										//
										afterLayout.add(new Span(cronFormatter.format(after)));
										//
										localNow = after;
									}
								}),
						"after");
			}
		}
		return ComponentView.of(layout);
	}

	public static <T> View render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@Nullable
		PandoraAccessLevel accessLevel;

		@NonNull
		Cron currentValue;

		@NonNull
		Cron initialValue;

		@NonNull
		Consumer<Cron> onValueChange;

		public static class Builder {
			// Lomboked
		}
	}
}
