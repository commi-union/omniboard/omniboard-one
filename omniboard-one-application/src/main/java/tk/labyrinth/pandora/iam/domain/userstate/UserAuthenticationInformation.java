package tk.labyrinth.pandora.iam.domain.userstate;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.Instant;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class UserAuthenticationInformation {

	@NonNull
	String displayName;

	@NonNull
	Instant expiresAt;

	//	/**
//	 * Instance of {@link OidcUser} for OAuth sources;
//	 */
	@Nullable
	Object securityObject;

	//	/**
//	 * - {@link ClientRegistration#getRegistrationId()} for OAuth sources;<br>
//	 */
	@NonNull
	String sourceId;

	/**
	 * Unique identifier for user in the context of current OAuth application.
	 */
	@NonNull
	String subject;
//	public static UserAuthenticationInformation fromOidcUser(String registrationId, OidcUser oidcUser) {
//		return UserAuthenticationInformation.builder()
//				.displayName(oidcUser.getName())
//				.expiresAt(oidcUser.getExpiresAt())
//				.securityObject(oidcUser)
//				.sourceId(registrationId)
//				.subject(oidcUser.getSubject())
//				.build();
//	}
}
