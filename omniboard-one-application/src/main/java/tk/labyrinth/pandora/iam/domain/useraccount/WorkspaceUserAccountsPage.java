package tk.labyrinth.pandora.iam.domain.useraccount;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardWorkspaceLayout;

@RequiredArgsConstructor
@Route(value = "useraccounts", layout = OmniboardWorkspaceLayout.class)
public class WorkspaceUserAccountsPage extends Composite<Component> {

	private final UserAccountsViewRenderer userAccountsViewRenderer;

	@Override
	protected Component initContent() {
		return userAccountsViewRenderer.render();
	}
}
