package tk.labyrinth.pandora.iam.domain.useraccount;

import jakarta.enterprise.context.ApplicationScoped;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.omniboardone.tool.RepositoryBase;

import java.util.UUID;

@ApplicationScoped
public class UserAccountRepository implements RepositoryBase<UserAccount> {

	@Nullable
	public UserAccount findByUid(UUID uid) {
		return find("_id", uid).firstResult();
	}

	@Nullable
	public UserAccount findByUsername(String username) {
		return find("username", username).firstResult();
	}
}
