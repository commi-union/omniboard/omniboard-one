package tk.labyrinth.pandora.iam.domain.login;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.Autocomplete;
import com.vaadin.flow.data.value.ValueChangeMode;
import jakarta.inject.Singleton;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.simple.secret.Secret;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.SpanRenderer;
import tk.labyrinth.pandora.functionalvaadin.sdk.ContainerWrapper;
import tk.labyrinth.pandora.iam.domain.useraccount.UserAccount;
import tk.labyrinth.pandora.iam.domain.useraccount.UserAccountRepository;
import tk.labyrinth.pandora.iam.domain.userstate.UserAuthenticationInformation;
import tk.labyrinth.pandora.iam.domain.userstate.UserState;
import tk.labyrinth.pandora.iam.domain.userstate.UserStateHandler;
import tk.labyrinth.pandora.misc4j.lib.spring.meta.LazyComponent;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.views.box.SecretBoxRenderer;
import tk.labyrinth.pandora.views.box.StringBoxRenderer;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

@LazyComponent
@RequiredArgsConstructor
@Singleton
public class PandoraLoginViewRenderer {

	private final UserAccountRepository userAccountRepository;

	private final UserStateHandler userStateHandler;

	public Component render() {
		return render(Parameters.Builder::build);
	}

	public Component render(Function<Parameters.Builder, Parameters> parametersConfigurer) {
		return render(parametersConfigurer.apply(Parameters.builder()));
	}

	public Component render(Parameters parameters) {
		ContainerWrapper<CssVerticalLayout> layout = FunctionalComponents.createContainer(CssVerticalLayout::new);
		{
			layout.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			layout.add(SpanRenderer.render("Sign In with Username/Password"));
			//
			layout.add(StringBoxRenderer.render(builder -> builder
					.currentValue(parameters.username())
					.label("Username")
					.onValueChange(parameters.onUsernameChange())
					.valueChangeMode(ValueChangeMode.EAGER)
					.build()));
			//
			layout.add(SecretBoxRenderer.render(builder -> builder
					.autocomplete(Autocomplete.CURRENT_PASSWORD)
					.currentValue(parameters.password())
					.label("Password")
					.onValueChange(parameters.onPasswordChange())
					.valueChangeMode(ValueChangeMode.EAGER)
					.build()));
			//
			layout.add(ButtonRenderer.render(builder -> builder
					.enabled(parameters.username() != null && parameters.password() != null)
					.onClick(event -> {
						UserAccount userAccount = userAccountRepository.findByUsername(parameters.username());
						//
						if (userAccount != null && Objects.equals(userAccount.getPassword(), parameters.password())) {
							userStateHandler.setUserState(UserState.builder()
									.authenticationInformation(UserAuthenticationInformation.builder()
											.displayName(userAccount.getDisplayName())
											.expiresAt(Instant.now().plus(Duration.ofHours(12)))
											.sourceId("pandora")
											.subject(userAccount.getUsername())
											.build())
									.build());
							//
							// TODO: Can it be smoother?
							UI.getCurrent().getPage().reload();
						} else {
							Notification.show("We don't know who you are", 2000, Notification.Position.TOP_CENTER);
						}
					})
					.text("Sign In")
					.themeVariants(ButtonVariant.LUMO_PRIMARY)
					.build()));
		}
		return layout.getContainer();
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		@NonNull
		Consumer<Secret> onPasswordChange;

		@NonNull
		Consumer<String> onUsernameChange;

		@Nullable
		Secret password;

		@Nullable
		String username;

		public static class Builder {
			// Lomboked
		}
	}
}
