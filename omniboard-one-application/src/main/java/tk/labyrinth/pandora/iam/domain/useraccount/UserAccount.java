package tk.labyrinth.pandora.iam.domain.useraccount;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import lombok.experimental.FieldDefaults;
import org.bson.codecs.pojo.annotations.BsonId;
import tk.labyrinth.omniboardone.common.persistence.HasUid;
import tk.labyrinth.pandora.datatypes.simple.secret.Secret;

import java.util.UUID;

@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@With
public class UserAccount implements HasUid<UserAccount> {

	String displayName;

	Secret password;

	@BsonId
	UUID uid;

	String username;
}
