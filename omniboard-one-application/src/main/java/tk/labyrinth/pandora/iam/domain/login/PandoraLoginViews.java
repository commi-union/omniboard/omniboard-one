package tk.labyrinth.pandora.iam.domain.login;

import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.datatypes.simple.secret.Secret;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponent2;
import tk.labyrinth.pandora.functionalvaadin.dialog.Dialogs;
import tk.labyrinth.pandora.reactive.observable.Observable;

@RequiredArgsConstructor
@Singleton
public class PandoraLoginViews {

	private final PandoraLoginViewRenderer pandoraLoginViewRenderer;

	public void showLoginDialog() {
		Observable<Pair<String, Secret>> usernamePasswordObservable = Observable.withInitialValue(Pair.of(null, null));
		//
		Dialogs.show(FunctionalComponent2.of(
				usernamePasswordObservable,
				(state, sink) -> pandoraLoginViewRenderer.render(builder -> builder
						.onPasswordChange(nextPassword -> sink.accept(Pair.of(state.getLeft(), nextPassword)))
						.onUsernameChange(nextUsername -> sink.accept(Pair.of(nextUsername, state.getRight())))
						.password(state.getRight())
						.username(state.getLeft())
						.build())));
	}
}
