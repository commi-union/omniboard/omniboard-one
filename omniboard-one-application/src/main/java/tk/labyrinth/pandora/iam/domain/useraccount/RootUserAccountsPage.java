package tk.labyrinth.pandora.iam.domain.useraccount;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardRootLayout;

@RequiredArgsConstructor
@Route(value = "useraccounts", layout = OmniboardRootLayout.class)
public class RootUserAccountsPage extends Composite<Component> {

	private final UserAccountsViewRenderer userAccountsViewRenderer;

	@Override
	protected Component initContent() {
		return userAccountsViewRenderer.render();
	}
}
