package tk.labyrinth.pandora.iam.domain.userstate;

import com.vaadin.quarkus.annotation.VaadinSessionScoped;
import lombok.NonNull;
import tk.labyrinth.pandora.reactive.observable.Observable;

@VaadinSessionScoped
public class UserStateHandler {

	private final Observable<UserState> userStateObservable = Observable.withInitialValue(UserState.builder()
			.authenticationInformation(null)
			.build());

	@NonNull
	public UserState getUserState() {
		return userStateObservable.get();
	}

	public void setUserState(@NonNull UserState userState) {
		userStateObservable.set(userState);
	}
}
