package tk.labyrinth.pandora.functions.domain.functionrun;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class FunctionRunUtils {

	private static final DateTimeFormatter toSecondsFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public static String formatToSeconds(Instant instant) {
		return toSecondsFormatter.format(instant.atOffset(ZoneOffset.UTC));
	}
}
