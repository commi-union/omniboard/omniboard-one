package tk.labyrinth.pandora.functions.domain.mathrun;

import jakarta.inject.Singleton;
import org.apache.commons.lang3.tuple.Pair;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.ClassBasedFunctionReference;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;
import tk.labyrinth.pandora.functions.domain.javafunction.JavaFunction;

@Singleton
public class MultiplyFunction implements JavaFunction<Pair<Integer, Integer>, Integer> {

	public static final TypedFunctionReference<Pair<Integer, Integer>, Integer> REFERENCE =
			new ClassBasedFunctionReference<>(MultiplyFunction.class);

	@Override
	public Integer execute(ExecutionContext context, Pair<Integer, Integer> parameters) {
		return parameters.getLeft() * parameters.getRight();
	}

	@Override
	public TypedFunctionReference<Pair<Integer, Integer>, Integer> getReference() {
		return REFERENCE;
	}
}
