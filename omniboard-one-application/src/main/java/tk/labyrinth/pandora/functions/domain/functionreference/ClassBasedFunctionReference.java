package tk.labyrinth.pandora.functions.domain.functionreference;

import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.functions.domain.javafunction.JavaFunction;
import tk.labyrinth.pandora.misc4j.java.lang.reflect.ParameterUtils;

import java.lang.reflect.Type;

@RequiredArgsConstructor
public class ClassBasedFunctionReference<P, R> implements TypedFunctionReference<P, R> {

	private final Class<? extends JavaFunction<P, R>> javaClass;

	@Override
	public String getName() {
		return javaClass.getSimpleName();
	}

	@Override
	public Type getParametersType() {
		return ParameterUtils.getFirstActualParameter(javaClass, JavaFunction.class);
	}

	@Override
	public Type getResultType() {
		return ParameterUtils.getSecondActualParameter(javaClass, JavaFunction.class);
	}
}
