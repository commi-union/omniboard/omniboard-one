package tk.labyrinth.pandora.functions.domain.functionrun;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import io.quarkus.panache.common.Sort;
import io.vavr.collection.List;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardRootLayout;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardWorkspaceLayout;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.domain.workspace.WorkspaceRepository;
import tk.labyrinth.omniboardone.util.RenderUtils;
import tk.labyrinth.omniboardone.util.ViewUtils;
import tk.labyrinth.pandora.datatypes.outcome.Outcome;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.TextAreaRenderer;
import tk.labyrinth.pandora.functionalvaadin.dialog.Dialogs;
import tk.labyrinth.pandora.functionalvaadin.view.fault.FaultViews;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.theme.LumoBadgeVariables;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

@RequiredArgsConstructor
@Route(value = "function-runs", layout = OmniboardWorkspaceLayout.class)
public class FunctionRunsPage extends Div implements BeforeEnterObserver {

	private final FunctionRunRepository functionRunRepository;

	private final ObjectMapper objectMapper;

	private final WorkspaceRepository workspaceRepository;

	private Component render(Workspace workspace) {
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
			//
			layout.setHeightFull();
		}
		{
			List<FunctionRunBase> functions = List.ofAll(functionRunRepository
					.findAll(Sort.by("startedAt", Sort.Direction.Descending))
					.page(0, 50)
					.stream());
			{
				TreeGrid<FunctionRunBase> grid = ViewUtils.createTreeGrid();
				{
					//
				}
				{
					grid
							.addHierarchyColumn(item -> "%s%s".formatted(
									item.getMultiRunIndex() != null ? "[%s] ".formatted(item.getMultiRunIndex()) : "",
									item.getFunctionName()))
							.setFlexGrow(3)
							.setHeader("Function")
							.setResizable(true);
					grid
							.addColumn(item -> FunctionRunUtils.formatToSeconds(item.getStartedAt()))
							.setHeader("Started At")
							.setResizable(true);
					grid
							.addColumn(item -> DurationFormatUtils.formatDuration(Duration
											.between(
													item.getStartedAt(),
													item.getFinishedAt() != null ? item.getFinishedAt() : Instant.now())
											.toMillis(),
									"HH:mm:ss.SSS"))
							.setHeader("Duration")
							.setResizable(true);
					grid
							.addComponentColumn(item -> renderStatusCell(item.getOutcome()))
							.setHeader("Status")
							.setResizable(true);
					grid
							.addComponentColumn(item -> Optional.ofNullable(item.getOutcome())
									.filter(Outcome::isSuccess)
									.map(Outcome::getResult)
									.map(result -> objectMapper.convertValue(result, JsonNode.class))
									.map(RenderUtils::renderJsonNode)
									.orElse(new Div()))
							.setFlexGrow(3)
							.setHeader("Result")
							.setResizable(true);
					grid
							.addComponentColumn(item -> item.getParameters() != null
									? RenderUtils.renderJsonNode(objectMapper.convertValue(item.getParameters(), JsonNode.class))
									: new Div())
							.setFlexGrow(3)
							.setHeader("Parameters")
							.setResizable(true);
					grid
							.addColumn(item -> item instanceof FunctionRun functionRun ? functionRun.getCreatedBy() : null)
							.setHeader("Created By")
							.setResizable(true);
				}
				{
					grid.setItems(
							functions.asJava(),
							item -> (item.getChildren() != null ? item.getChildren() : List.<FunctionRunBase>empty())
									.map(FunctionRunBase.class::cast)
									.asJava());
				}
				layout.add(grid);
			}
		}
		return layout;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		Component component = Try.ofSupplier(() -> {
					Workspace workspace = workspaceRepository.findByReference(event
							.getRouteParameters()
							.get(OmniboardRootLayout.WORKSPACE_REFERENCE_NAME)
							.orElse(null));
					//
					return render(workspace);
				})
				.recover(FaultViews::createView)
				.get();
		//
		removeAll();
		//
		add(component);
	}

	public static Component renderStatusCell(@Nullable Outcome<?> outcome) {
		Component result;
		{
			if (outcome != null) {
				if (outcome.isSuccess()) {
					CssHorizontalLayout layout = new CssHorizontalLayout();
					{
						layout.add(RenderUtils.renderBadge("Success", LumoBadgeVariables.SUCCESS));
						//
						layout.add(ButtonRenderer.render(builder -> builder
								.icon(VaadinIcon.EYE.create())
								.onClick(event -> Dialogs.show(TextAreaRenderer.render(resultAreaBuilder -> resultAreaBuilder
										.readOnly(true)
										.value(outcome.getResult() != null ? outcome.getResult().toString() : null)
										.build())))
								.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
								.build()));
					}
					result = layout;
				} else {
					CssHorizontalLayout layout = new CssHorizontalLayout();
					{
						layout.add(RenderUtils.renderBadge("Failure", LumoBadgeVariables.ERROR));
						//
						layout.add(ButtonRenderer.render(builder -> builder
								.icon(VaadinIcon.EYE.create())
								.onClick(event -> FaultViews.showDialog(outcome.getFault()))
								.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
								.build()));
					}
					result = layout;
				}
			} else {
				result = RenderUtils.renderBadge("Ongoing", null);
			}
		}
		return result;
	}
}
