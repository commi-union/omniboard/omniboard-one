package tk.labyrinth.pandora.functions.domain.functionrun;

import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import lombok.experimental.FieldDefaults;
import org.bson.codecs.pojo.annotations.BsonId;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.outcome.Outcome;

import java.time.Instant;
import java.util.UUID;

@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@With
public class FunctionRun implements FunctionRunBase {

	List<ChildFunctionRun> children;

	// TODO: Trigger (user/routine/api).
	String createdBy;

	@Nullable
	Instant finishedAt;

	// TODO: Reference
	String functionName;

	@Nullable
	Outcome<?> outcome;

	Object parameters;

	// TODO: Add scheduledAt
	Instant startedAt;

	@BsonId
	UUID uid;

	@Nullable
	@Override
	public Integer getMultiRunIndex() {
		return null;
	}
}
