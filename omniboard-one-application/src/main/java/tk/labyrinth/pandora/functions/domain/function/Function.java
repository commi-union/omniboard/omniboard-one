package tk.labyrinth.pandora.functions.domain.function;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import lombok.experimental.FieldDefaults;
import org.bson.codecs.pojo.annotations.BsonId;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;

import java.util.UUID;

@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@With
public class Function {

	String name;

	ObjectModel parametersModel;

	ObjectModel resultModel;

	@BsonId
	UUID uid;
}
