package tk.labyrinth.pandora.functions.domain.functionexecution;

import io.vavr.collection.List;
import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class Path<T> {

	@NonNull
	List<T> segments;

	public Path<T> append(T segment) {
		return of(segments.append(segment));
	}

	public T getFirst() {
		return segments.get(0);
	}

	public int size() {
		return segments.size();
	}

	public Path<T> tail() {
		return of(segments.tail());
	}

	public static <T> Path<T> empty() {
		return of(List.empty());
	}

	public static <T> Path<T> first(T firstSegment) {
		return of(List.of(firstSegment));
	}
}
