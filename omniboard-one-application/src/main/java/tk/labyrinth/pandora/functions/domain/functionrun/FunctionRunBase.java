package tk.labyrinth.pandora.functions.domain.functionrun;

import io.vavr.collection.List;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.outcome.Outcome;

import java.time.Instant;

public interface FunctionRunBase {

	List<? extends FunctionRunBase> getChildren();

	Instant getFinishedAt();

	String getFunctionName();

	@Nullable
	Integer getMultiRunIndex();

	Outcome<?> getOutcome();

	Object getParameters();

	Instant getStartedAt();
}
