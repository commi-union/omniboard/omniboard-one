package tk.labyrinth.pandora.functions.domain.function;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Try;
import jakarta.enterprise.inject.Instance;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardRootLayout;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardWorkspaceLayout;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.domain.workspace.WorkspaceRepository;
import tk.labyrinth.omniboardone.iam.AccessManager;
import tk.labyrinth.omniboardone.util.ViewUtils;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.fault.FaultViews;
import tk.labyrinth.pandora.iam.domain.userstate.UserStateHandler;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.views.style.PandoraStyles;

@RequiredArgsConstructor
@Route(value = "functions", layout = OmniboardWorkspaceLayout.class)
public class FunctionsPage extends Div implements BeforeEnterObserver {

	private final AccessManager accessManager;

	private final FunctionManager functionManager;

	private final Instance<FunctionParametersViewProvider> functionParametersViewProviders;

	private final FunctionRegistry functionRegistry;

	private final UserStateHandler userStateHandler;

	private final WorkspaceRepository workspaceRepository;

	private Component render(Workspace workspace) {
		boolean isAdmin = accessManager.isCurrentUserAdmin();
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
		}
		{
			List<Function> functions = functionRegistry.getFunctions();
			{
				Grid<Function> grid = ViewUtils.createGrid();
				{
					grid
							.addColumn(Function::getName)
							.setHeader("Name")
							.setResizable(true);
					grid
							.addColumn(Function::getParametersModel)
							.setHeader("Parameters Model")
							.setResizable(true);
					grid
							.addColumn(Function::getResultModel)
							.setHeader("Result Model")
							.setResizable(true);
					grid
							.addComponentColumn(item -> {
								FunctionParametersViewProvider viewProvider = List.ofAll(functionParametersViewProviders)
										.filter(provider -> provider.supports(item))
										.headOption()
										.getOrNull();
								//
								Map<String, Object> context = HashMap.of("workspace", workspace);
								//
								return ButtonRenderer.render(builder -> builder
										.enabled(isAdmin && viewProvider != null)
										.onClick(event -> viewProvider.showFunctionParametersView(context, item, null)
												.subscribeAlwaysAccepted(result -> functionManager.runFunctionAsync(
														"user:%s".formatted(
																userStateHandler.getUserState().getAuthenticationInformation().getSubject()),
														item.getName(),
														result)))
										.text("Run")
										.themeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL)
										.build());
							})
							.setFlexGrow(0);
				}
				{
					grid.setItems(functions.asJava());
				}
				layout.add(grid);
			}
		}
		return layout;
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		Component component = Try.ofSupplier(() -> {
					Workspace workspace = workspaceRepository.findByReference(event
							.getRouteParameters()
							.get(OmniboardRootLayout.WORKSPACE_REFERENCE_NAME)
							.orElse(null));
					//
					return render(workspace);
				})
				.recover(FaultViews::createView)
				.get();
		//
		removeAll();
		//
		add(component);
	}
}
