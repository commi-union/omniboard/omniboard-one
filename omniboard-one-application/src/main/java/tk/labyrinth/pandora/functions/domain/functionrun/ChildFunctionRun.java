package tk.labyrinth.pandora.functions.domain.functionrun;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.outcome.Outcome;

import java.time.Instant;

@Builder(builderClassName = "Builder", toBuilder = true)
@Value
@With
public class ChildFunctionRun implements FunctionRunBase {

	List<ChildFunctionRun> children;

	@Nullable
	Instant finishedAt;

	// TODO: Reference
	String functionName;

	@Nullable
	Integer multiRunIndex;

	@Nullable
	Outcome<?> outcome;

	Object parameters;

	Instant startedAt;
}
