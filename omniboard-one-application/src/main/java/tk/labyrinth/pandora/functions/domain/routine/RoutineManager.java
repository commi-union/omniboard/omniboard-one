package tk.labyrinth.pandora.functions.domain.routine;

import com.cronutils.model.Cron;
import io.quarkus.runtime.StartupEvent;
import io.quarkus.scheduler.Scheduled;
import io.quarkus.scheduler.Scheduler;
import io.quarkus.scheduler.Trigger;
import io.vavr.collection.List;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import tk.labyrinth.pandora.functions.domain.function.FunctionManager;

import java.time.ZoneOffset;
import java.util.UUID;

@RequiredArgsConstructor
@Singleton
@Slf4j
public class RoutineManager {

	private final FunctionManager functionManager;

	private final RoutineRepository routineRepository;

	private final Scheduler scheduler;

	private void alignScheduledJob(Routine routine) {
		Trigger previousTrigger = scheduler.unscheduleJob(routine.getUid().toString());
		//
		boolean enabled = BooleanUtils.isTrue(routine.getEnabled());
		//
		if (enabled) {
			Cron cron = routine.getCron();
			String functionName = routine.getFunctionName();
			//
			if (cron != null && functionName != null) {
				Scheduler.JobDefinition jobDefinition = scheduler.newJob(routine.getUid().toString())
						.setCron(cron.asString())
						.setTask(execution -> {
							logger.info("Running Routine Function '{}' ({})", routine.getName(), routine.getUid());
							//
							try {
								functionManager.runFunction(
										"routine:%s".formatted(routine.getUid()),
										functionName,
										routine.getParameters());
							} catch (RuntimeException ex) {
								logger.error("", ex);
							}
						})
						.setTimeZone(ZoneOffset.UTC.getId());
				//
				//
				Trigger nextTrigger = jobDefinition.schedule();
				//
				if (previousTrigger != null) {
					logger.info("Rescheduled Routine '{}' ({}) with next execution at {}",
							routine.getName(), routine.getUid(), nextTrigger.getNextFireTime());
				} else {
					logger.info("Scheduled Routine '{}' ({}) with next execution at {}",
							routine.getName(), routine.getUid(), nextTrigger.getNextFireTime());
				}
			} else {
				logger.warn("Routine '{}' ({}) is not scheduled due to missing attributes: cron = {}, functionName = {}",
						routine.getName(), routine.getUid(), cron, functionName);
			}
		} else {
			if (previousTrigger != null) {
				logger.info("Unscheduled Routine '{}' ({})", routine.getName(), routine.getUid());
			}
		}
	}

	private void onStartup(@Observes StartupEvent ev) {
		List<Routine> enabledRoutines = routineRepository.findAllByEnabled(true);
		//
		enabledRoutines.forEach(this::alignScheduledJob);
	}

	public Trigger getTrigger(UUID routineUid) {
		return scheduler.getScheduledJob(routineUid.toString());
	}

	@Scheduled(cron = "0 0 * * * *")
	public void sched() {
		System.out.println("HELLO");
	}

	public void updateRoutine(Routine routine) {
		alignScheduledJob(routine);
	}
}
