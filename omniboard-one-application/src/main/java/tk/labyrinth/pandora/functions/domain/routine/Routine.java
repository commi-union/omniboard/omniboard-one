package tk.labyrinth.pandora.functions.domain.routine;

import com.cronutils.model.Cron;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import lombok.experimental.FieldDefaults;
import org.bson.codecs.pojo.annotations.BsonId;

import java.time.Instant;
import java.util.UUID;

@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@With
public class Routine {

	Cron cron;

	Instant effectiveSince;

	Boolean enabled;

	String functionName;

	String name;

	Object parameters;

	@BsonId
	UUID uid;
}
