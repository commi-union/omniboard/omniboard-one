package tk.labyrinth.pandora.functions.domain.javafunction;

import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;

/**
 * @param <P> Parameters
 * @param <R> Result
 */
public interface JavaFunction<P, R> {

	TypedFunctionReference<P, R> getReference();

	R execute(ExecutionContext context, P parameters);
}
