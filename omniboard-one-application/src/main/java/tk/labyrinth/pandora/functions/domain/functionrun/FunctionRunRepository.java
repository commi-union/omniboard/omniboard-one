package tk.labyrinth.pandora.functions.domain.functionrun;

import io.vavr.collection.List;
import jakarta.inject.Singleton;
import tk.labyrinth.omniboardone.tool.RepositoryBase;

import java.time.Instant;

@Singleton
public class FunctionRunRepository implements RepositoryBase<FunctionRun> {

	public List<FunctionRun> findAllByStartedAtBeforeAndFinishedAtNotNull(Instant startedAt) {
//		return List.ofAll(find("startedAt < '?1' AND finishedAt = null", startedAt).list());
		return List.ofAll(find("{ finishedAt: { $exists: false }, startedAt: { $lt: '?1' } }", startedAt).list());
	}
}
