package tk.labyrinth.pandora.functions.domain.routine;

import jakarta.inject.Singleton;
import tk.labyrinth.omniboardone.tool.RepositoryBase;

@Singleton
public class RoutineRepository implements RepositoryBase<Routine> {
	// empty
}
