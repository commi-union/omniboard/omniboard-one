package tk.labyrinth.pandora.functions.domain.javafunction;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.util.Pair;
import io.quarkus.runtime.StartupEvent;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import jakarta.enterprise.event.Observes;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.datatypes.objectmodel.ObjectModel;
import tk.labyrinth.pandora.datatypes.outcome.Outcome;
import tk.labyrinth.pandora.functions.domain.function.Function;
import tk.labyrinth.pandora.functions.domain.function.FunctionExecutor;
import tk.labyrinth.pandora.functions.domain.function.FunctionRegistry;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;

import java.lang.reflect.Type;
import java.util.Objects;
import java.util.UUID;

@RequiredArgsConstructor
@Singleton
public class JavaFunctionExecutor implements FunctionExecutor {

	private final FunctionRegistry functionRegistry;

	private final Instance<JavaFunction<?, ?>> javaFunctions;

	private final ObjectMapper objectMapper;

	private Map<Function, JavaFunction<?, ?>> functionToJavaFunctionMap;

	@SuppressWarnings("unchecked")
	private <P, R> Outcome<R> doExecuteFunction(ExecutionContext context, Function function, P parameters) {
		JavaFunction<P, R> javaFunction = (JavaFunction<P, R>) functionToJavaFunctionMap.get(function).get();
		//
		R result = javaFunction.execute(context, parameters);
		//
		return Outcome.ofResult(result);
	}

	private void onStartup(@Observes StartupEvent ignored) {
		functionToJavaFunctionMap = List.ofAll(javaFunctions)
				.map(javaFunction -> {
					Function function = registerJavaFunction(javaFunction);
					//
					return Pair.of(function, javaFunction);
				})
				.toMap(Pair::getLeft, Pair::getRight);
	}

	@Override
	public Outcome<Object> executeFunction(ExecutionContext context, Function function, Object parameters) {
		return doExecuteFunction(context, function, parameters);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <P, R> TypedFunctionReference<P, R> getFunctionReference(String functionName) {
		return (TypedFunctionReference<P, R>) functionToJavaFunctionMap
				.find(tuple -> Objects.equals(tuple._1().getName(), functionName))
				.get()
				._2()
				.getReference();
	}

	public Function registerJavaFunction(JavaFunction<?, ?> javaFunction) {
		Function function = Function.builder()
				.name(javaFunction.getClass().getSimpleName())
				.parametersModel(composeParametersModel(javaFunction.getReference().getParametersType()))
				.resultModel(null)
				.uid(UUID.randomUUID())
				.build();
		//
		functionRegistry.registerFunction(function, this);
		//
		return function;
	}

	@Nullable
	public static ObjectModel composeParametersModel(Type parametersJavaType) {
		return parametersJavaType != Void.class
				//
				// TODO
				? ObjectModel.builder()
				.build()
				: null;
	}
}
