package tk.labyrinth.pandora.functions.domain.functionrun;

import io.vavr.collection.List;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.pandora.datatypes.fault.StructuredFault;
import tk.labyrinth.pandora.datatypes.outcome.Outcome;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.ClassBasedFunctionReference;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;
import tk.labyrinth.pandora.functions.domain.javafunction.JavaFunction;

import java.time.Duration;
import java.time.Instant;

@RequiredArgsConstructor
@Singleton
public class FinishTimedOutFunctionRunsFunction implements JavaFunction<Void, Integer> {

	public static final TypedFunctionReference<Void, Integer> REFERENCE =
			new ClassBasedFunctionReference<>(FinishTimedOutFunctionRunsFunction.class);

	private final FunctionRunRepository functionRunRepository;

	@Override
	public Integer execute(ExecutionContext context, Void parameters) {
		List<FunctionRun> timedOutFunctionRuns = functionRunRepository.findAllByStartedAtBeforeAndFinishedAtNotNull(
				Instant.now().minus(Duration.ofHours(1)));
		//
		timedOutFunctionRuns.forEach(timedOutFunctionRun -> {
			functionRunRepository.update(timedOutFunctionRun.toBuilder()
					.finishedAt(Instant.now())
					.outcome(Outcome.ofFault(StructuredFault.of("Timed out")))
					.build());
		});
		//
		return timedOutFunctionRuns.size();
	}

	@Override
	public TypedFunctionReference<Void, Integer> getReference() {
		return REFERENCE;
	}
}
