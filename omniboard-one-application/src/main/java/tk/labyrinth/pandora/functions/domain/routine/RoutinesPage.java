package tk.labyrinth.pandora.functions.domain.routine;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import io.quarkus.scheduler.Trigger;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Try;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.inject.Instance;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardRootLayout;
import tk.labyrinth.omniboardone.domain.sidebar.OmniboardWorkspaceLayout;
import tk.labyrinth.omniboardone.domain.workspace.Workspace;
import tk.labyrinth.omniboardone.domain.workspace.WorkspaceRepository;
import tk.labyrinth.omniboardone.iam.AccessManager;
import tk.labyrinth.omniboardone.util.RenderUtils;
import tk.labyrinth.omniboardone.util.ViewUtils;
import tk.labyrinth.pandora.datatypes.simple.access.PandoraAccessLevel;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.component.ButtonRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.CheckboxRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.SelectRenderer;
import tk.labyrinth.pandora.functionalvaadin.component.TextFieldRenderer;
import tk.labyrinth.pandora.functionalvaadin.sdk.ContainerWrapper;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.functionalvaadin.view.fault.FaultViews;
import tk.labyrinth.pandora.functions.domain.function.Function;
import tk.labyrinth.pandora.functions.domain.function.FunctionParametersViewProvider;
import tk.labyrinth.pandora.functions.domain.function.FunctionRegistry;
import tk.labyrinth.pandora.functions.domain.functionrun.FunctionRunUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.views.CronBoxRenderer;
import tk.labyrinth.pandora.views.box.BoxRendererRegistry;
import tk.labyrinth.pandora.views.box.StringBoxRenderer;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.time.Instant;
import java.util.Comparator;
import java.util.Objects;
import java.util.UUID;

@RequiredArgsConstructor
@Route(value = "routines", layout = OmniboardWorkspaceLayout.class)
public class RoutinesPage extends Div implements BeforeEnterObserver {

	private final AccessManager accessManager;

	private final BoxRendererRegistry boxRendererRegistry;

	private final Instance<FunctionParametersViewProvider> functionParametersViewProviders;

	private final FunctionRegistry functionRegistry;

	private final RoutineManager routineManager;

	private final RoutineRepository routineRepository;

	private final WorkspaceRepository workspaceRepository;
//	@All
//	private java.util.List<BoxRendererContributor<?>> boxRendererContributors;

	@PostConstruct
	private void postConstruct() {
		System.out.println();
	}

	private Component render(Workspace workspace) {
		boolean isAdmin = accessManager.isCurrentUserAdmin();
		//
		CssVerticalLayout layout = new CssVerticalLayout();
		{
			layout.addClassName(PandoraStyles.LAYOUT);
		}
		{
			List<Routine> routines = routineRepository
					.findAllAsList()
					.sorted(Comparator.comparing(Routine::getEffectiveSince).reversed());
			{
				layout.addHorizontalLayout(toolbar -> {
					{
						toolbar.addClassName(PandoraStyles.LAYOUT);
					}
					{
						toolbar.addStretch();
						//
						toolbar.add(ButtonRenderer.render(builder -> builder
								.enabled(isAdmin)
								.onClick(event -> showEditDialog(workspace, Routine.builder().build()))
								.text("Add")
								.themeVariants(ButtonVariant.LUMO_PRIMARY)
								.build()));
					}
				});
			}
			{
				Grid<Routine> grid = ViewUtils.createGrid();
				{
					grid
							.addColumn(Routine::getName)
							.setHeader("Name")
							.setResizable(true);
					grid
							.addColumn(Routine::getFunctionName)
							.setHeader("Function")
							.setResizable(true);
					grid
							.addColumn(Routine::getParameters)
							.setHeader("Parameters")
							.setResizable(true);
					grid
							.addColumn(item -> item.getCron() != null ? item.getCron().asString() : null)
							.setHeader("Cron")
							.setResizable(true);
					grid
							.addColumn(item -> {
								Trigger trigger = routineManager.getTrigger(item.getUid());
								//
								return trigger != null ? FunctionRunUtils.formatToSeconds(trigger.getNextFireTime()) : null;
							})
							.setHeader("Next Run")
							.setResizable(true);
					grid
							.addColumn(Routine::getEnabled)
							.setHeader("Enabled")
							.setResizable(true);
					grid
							.addColumn(item -> RenderUtils.format(item.getEffectiveSince()))
							.setHeader("Effective Since")
							.setResizable(true);
					grid
							.addComponentColumn(item -> ButtonRenderer.render(builder -> builder
									.icon(VaadinIcon.EDIT.create())
									.onClick(event -> showEditDialog(workspace, item))
									.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY)
									.build()))
							.setFlexGrow(0)
							.setResizable(false);
				}
				{
					grid.setItems(routines.asJava());
				}
				layout.add(grid);
			}
		}
		return layout;
	}

	private void showEditDialog(@NonNull Workspace workspace, @NonNull Routine object) {
		boolean isAdmin = accessManager.isCurrentUserAdmin();
		//
		ConfirmationViews
				.showViewFunctionDialog(
						object,
						(next, sink) -> {
							List<Function> functions = functionRegistry.getFunctions();
							Function function = next.getFunctionName() != null
									? functions
									.filter(functionElement -> Objects.equals(functionElement.getName(), next.getFunctionName()))
									.getOrNull()
									: null;
							FunctionParametersViewProvider parametersViewProvider = function != null
									? List.ofAll(functionParametersViewProviders)
									.filter(provider -> provider.supports(function))
									.headOption()
									.getOrNull()
									: null;
							//
							ContainerWrapper<CssVerticalLayout> layout = FunctionalComponents.createContainer(CssVerticalLayout::new);
							{
								layout.addClassName(PandoraStyles.LAYOUT);
							}
							{
								{
									// FIXME
									layout.getContainer().getChildren()
											.filter(H3.class::isInstance)
											.forEach(Component::removeFromParent);
									layout.getElement().insertChild(
											0,
											new H3("%s Routine".formatted(next.getUid() != null ? "Edit" : "Create")).getElement());
								}
								{
									ContainerWrapper<CssVerticalLayout> formLayout = FunctionalComponents
											.createContainer(CssVerticalLayout::new);
									{
										formLayout.addClassName(PandoraStyles.LAYOUT);
									}
									{
										formLayout.add(StringBoxRenderer.render(builder -> builder
												.accessLevel(isAdmin ? PandoraAccessLevel.EDITABLE : PandoraAccessLevel.VIEWABLE)
												.currentValue(next.getName())
												.initialValue(object.getName())
												.label("Name")
												.onValueChange(nextValue -> sink.accept(next.withName(nextValue)))
												.build()));
										//
										formLayout.add(SelectRenderer.<String>render(builder -> builder
												.items(functions
														.map(Function::getName)
														.asJava())
												.label("Function")
												.onValueChange(nextValue -> sink.accept(next.withFunctionName(nextValue)))
												.readOnly(!isAdmin)
												.value(next.getFunctionName())
												.build()));
										//
										formLayout.add(TextFieldRenderer.render(builder -> builder
												.label("Parameters")
												.readOnly(true)
												.suffixComponent(ButtonRenderer.render(buttonBuilder -> buttonBuilder
														.enabled(isAdmin && parametersViewProvider != null)
														.icon(VaadinIcon.COG.create())
														.onClick(event -> {
															Map<String, Object> context = HashMap.of("workspace", workspace);
															//
															parametersViewProvider
																	.showFunctionParametersView(context, function, next.getParameters())
																	.subscribeAlwaysAccepted(result -> sink.accept(next.withParameters(result)));
														})
														.themeVariants(ButtonVariant.LUMO_SMALL, ButtonVariant.LUMO_TERTIARY_INLINE)
														.build()))
												.value(next.getParameters() != null ? next.getParameters().toString() : "")
												.build()));
										//
										formLayout.add(CronBoxRenderer
												.render(builder -> builder
														.accessLevel(isAdmin ? PandoraAccessLevel.EDITABLE : PandoraAccessLevel.VIEWABLE)
														.currentValue(next.getCron())
														.initialValue(object.getCron())
														.label("Cron")
														.onValueChange(nextValue -> sink.accept(next.withCron(nextValue)))
														.build())
												.asVaadinComponent());
										//
										formLayout.add(CheckboxRenderer.render(builder -> builder
												.enabled(isAdmin)
												.label("Enabled")
												.onValueChange(nextValue -> sink.accept(next.withEnabled(nextValue)))
												.value(next.getEnabled() != null ? next.getEnabled() : false)
												.build()));
									}
									layout.add(formLayout.getContainer());
								}
								return layout.asView();
							}
						})
				.subscribeAlwaysAccepted(result -> {
					if (isAdmin) {
						Routine routine = result.toBuilder()
								.effectiveSince(Instant.now())
								.uid(result.getUid() != null ? result.getUid() : UUID.randomUUID())
								.build();
						//
						routineRepository.persistOrUpdate(routine);
						//
						// FIXME: This should be a manipulation listener.
						routineManager.updateRoutine(routine);
						//
						// TODO: We just need to refresh the content here.
						UI.getCurrent().getPage().reload();
					}
				});
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		Component component = Try.ofSupplier(() -> {
					Workspace workspace = workspaceRepository.findByReference(event
							.getRouteParameters()
							.get(OmniboardRootLayout.WORKSPACE_REFERENCE_NAME)
							.orElse(null));
					//
					return render(workspace);
				})
				.recover(FaultViews::createView)
				.get();
		//
		removeAll();
		//
		add(component);
	}
}
