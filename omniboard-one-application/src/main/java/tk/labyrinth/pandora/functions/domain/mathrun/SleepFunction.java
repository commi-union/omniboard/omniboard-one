package tk.labyrinth.pandora.functions.domain.mathrun;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.H3;
import io.vavr.collection.Map;
import jakarta.inject.Singleton;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.experimental.Accessors;
import org.checkerframework.checker.nullness.qual.Nullable;
import tk.labyrinth.pandora.functionalvaadin.FunctionalComponents;
import tk.labyrinth.pandora.functionalvaadin.component.IntegerFieldRenderer;
import tk.labyrinth.pandora.functionalvaadin.view.ComponentView;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationHandle;
import tk.labyrinth.pandora.functionalvaadin.view.confirmation.ConfirmationViews;
import tk.labyrinth.pandora.functions.domain.function.Function;
import tk.labyrinth.pandora.functions.domain.function.FunctionParametersViewProvider;
import tk.labyrinth.pandora.functions.domain.functionexecution.ExecutionContext;
import tk.labyrinth.pandora.functions.domain.functionreference.ClassBasedFunctionReference;
import tk.labyrinth.pandora.functions.domain.functionreference.TypedFunctionReference;
import tk.labyrinth.pandora.functions.domain.javafunction.JavaFunction;
import tk.labyrinth.pandora.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.views.style.PandoraStyles;

import java.util.Objects;

@RequiredArgsConstructor
@Singleton
public class SleepFunction implements FunctionParametersViewProvider, JavaFunction<SleepFunction.Parameters, Void> {

	public static final TypedFunctionReference<Parameters, Void> REFERENCE =
			new ClassBasedFunctionReference<>(SleepFunction.class);

	private final ObjectMapper objectMapper;

	@Override
	public Void execute(ExecutionContext context, Parameters parameters) {
		ThreadUtils.sleep(parameters.durationMs());
		//
		return null;
	}

	@Override
	public TypedFunctionReference<Parameters, Void> getReference() {
		return REFERENCE;
	}

	@Override
	public ConfirmationHandle<Object> showFunctionParametersView(
			Map<String, Object> context,
			Function function,
			@Nullable Object parameters) {
		Parameters castedParameters = objectMapper.convertValue(parameters, Parameters.class);
		//
		return ConfirmationViews
				.showViewFunctionDialog(
						castedParameters != null ? castedParameters : Parameters.builder().build(),
						(next, sink) -> {
							CssVerticalLayout layout = FunctionalComponents.createComponent(CssVerticalLayout::new);
							{
								layout.addClassName(PandoraStyles.LAYOUT);
							}
							{
								{
									layout.getChildren().filter(H3.class::isInstance).forEach(Component::removeFromParent);
									layout.add(new H3("Parameters"));
								}
								{
									layout.add(IntegerFieldRenderer.render(builder -> builder
											.label("Duration (ms)")
											.onValueChange(nextValue -> sink.accept(
													next.withDurationMs(nextValue != null ? nextValue.longValue() : null)))
											.value(next.durationMs() != null ? next.durationMs().intValue() : null)
											.build()));
								}
							}
							return ComponentView.of(layout);
						})
				.map(Object.class::cast);
	}

	@Override
	public boolean supports(Function function) {
		return Objects.equals(function.getName(), getClass().getSimpleName());
	}

	@Accessors(fluent = true)
	@Builder(builderClassName = "Builder", toBuilder = true)
	@Value
	@With
	public static class Parameters {

		Long durationMs;
	}
}
